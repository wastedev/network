package com.infinity.parser;

import com.google.common.collect.Lists;
import me.adamtanana.core.command.Command;
import me.adamtanana.core.engine.GameType;
import me.adamtanana.core.player.Rank;
import me.adamtanana.core.util.C;
import me.adamtanana.core.util.UtilWorld;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.List;

public class ContinueCommand extends Command {
    public ContinueCommand() {
        super("continue", Lists.newArrayList(), 2, Rank.BUILDER);
    }

    @Override
    public void execute(Player player, List<String> list) {
        if (Bukkit.getWorld(list.get(1)) != null) {
            player.sendMessage(C.red + "This world is already loaded");
            player.teleport(Bukkit.getWorld(list.get(1)).getBlockAt(0, 60, 0).getLocation());
            return;
        }

        try {
            if (GameType.valueOf(list.get(0)) == null) {
                return;
            }
        } catch (Exception e) {
            player.sendMessage(C.red + "You must use a valid game type. These include: ");
            StringBuilder msg = new StringBuilder("");
            for (GameType gameType : GameType.values()) {
                msg.append(gameType.name() + ", ");
            }
            msg.setLength(msg.length() - 2);
            player.sendMessage(C.darkAqua + msg.toString());
            return;
        }


        for(List<String> game : UtilWorld.getAllBuildMaps().values()) {
            if(game.contains(list.get(1))) {
                UtilWorld.loadBuildWorld(list.get(0), list.get(1), worldData -> {
                    player.teleport(worldData.getWorld().getSpawnLocation());
                    Parser.getGametypes().put(worldData.getWorld(), list.get(0));
                    player.sendMessage(C.red + "Loaded");
                });
                return;
            }
        }

        player.sendMessage(C.red + "That map exists! Use /maps to see all maps");



    }
}
