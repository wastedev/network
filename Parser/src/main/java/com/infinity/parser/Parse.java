package com.infinity.parser;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import me.adamtanana.core.event.UpdateEvent;
import me.adamtanana.core.event.UpdateType;
import me.adamtanana.core.util.*;
import org.apache.commons.io.FileUtils;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Sign;
import org.bukkit.event.Listener;
import org.bukkit.util.FileUtil;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class Parse implements Listener {
    private World world;
    private int x = 0, y = 0, z = 0;
    private int size = 500;
    private int processed = 0;
    private File mapLoc;
    private String gametype = "";
    private boolean ready = false;


    /** Sponge for corners **/
    /** Wool For data and spawn points **/
    /**
     * IRON   GOLD
     **/
    private List<Location> corners = Lists.newArrayList();
    private HashMap<DyeColor, List<Location>> spawnLocs = Maps.newHashMap();
    private HashMap<DyeColor, List<Location>> dataLocs = Maps.newHashMap();
    private HashMap<String, List<Location>> signLocs = Maps.newHashMap();

    public Parse(World world, int count) {
        this.world = world;
        size = count;
        x = -size;
        z = -size;
        y = 0;
        gametype = Parser.getGametypes().remove(world);
        mapLoc = new File(UtilWorld.getMapFolder(), gametype + "/" + world.getName());
        Bukkit.broadcastMessage(C.darkAqua + "Starting parse of " + world.getName());
        UtilWorld.saveBuildWorld(world.getName(), gametype);
        UtilServer.registerListener(this);
        UtilServer.runTaskLater(() -> ready = true, 60L); //Let world save
    }

    public boolean update() {
        if (!ready) return false;

        long start = System.currentTimeMillis();
        for (; x <= size; x++) {
            for (; z <= size; z++) {
                for (; y <= 256; y++) {
                    if (UtilTime.elapsed(start, 10)) return false;

                    processed++;
                    if (processed % 5000000 == 0)
                        Bukkit.broadcastMessage("Scanning World: " + (processed / 1000000) + "M of " +
                                (((size * 2) * (size * 2) * 256) / 1000000) + "M");


                    Block b = world.getBlockAt(x, y, z);
                    if (b == null || b.getType() == null || b.getType() == Material.AIR) continue;
                    Block above = b.getRelative(BlockFace.UP);
                    if (above == null || above.getType() == null || above.getType() == Material.AIR) continue;

                    if (b.getType().equals(Material.WOOL)) {
                        if (above.getType() == Material.IRON_PLATE) {
                            Location l = b.getLocation();
                            DyeColor color = DyeColor.getByData(b.getData());
                            if (dataLocs.containsKey(color)) dataLocs.get(color).add(l);
                            else {
                                dataLocs.put(color, new ArrayList<>());
                                dataLocs.get(color).add(l);
                            }
                            System.out.println("Data locs new one " + DyeColor.getByData(b.getData()).name() + " now data has " + dataLocs.size() + " colors");
                            UtilWorld.quickChangeBlock(world, b.getLocation(), 0, 0);
                            UtilWorld.quickChangeBlock(world, b.getRelative(BlockFace.UP).getLocation(), 0, 0);
                        } else if (above.getType() == Material.GOLD_PLATE) {
                            Location l = b.getLocation();
                            DyeColor color = DyeColor.getByData(b.getData());
                            if (spawnLocs.containsKey(color)) spawnLocs.get(color).add(l);
                            else {
                                spawnLocs.put(color, new ArrayList<>());
                                spawnLocs.get(color).add(l);
                            }
                            System.out.println("Spawns locs new one " + DyeColor.getByData(b.getData()).name() + " now Spawns has " + spawnLocs.size() + " colors");

                            UtilWorld.quickChangeBlock(world, b.getLocation(), 0, 0);
                            UtilWorld.quickChangeBlock(world, b.getRelative(BlockFace.UP).getLocation(), 0, 0);
                        } else if (above.getType() == Material.SIGN_POST) {
                            Sign s = (Sign) above.getState();
                            Location loc = s.getBlock().getRelative(BlockFace.DOWN).getLocation();
                            String msg = s.getLine(0);
                            if (signLocs.containsKey(msg)) {
                                signLocs.get(msg).add(loc);
                            } else {
                                List<Location> locs = Lists.newArrayList();
                                locs.add(loc);
                                signLocs.put(msg, locs);
                            }
                            System.out.println("Custom locs new one '" + ((Sign) above.getState()).getLine(0) + "' now Spawns has " + signLocs.size() + " ones");

                            UtilWorld.quickChangeBlock(world, s.getLocation(), 0, 0);
                            UtilWorld.quickChangeBlock(world, b.getLocation(), 0, 0);
                        }
                    } else if (b.getType().equals(Material.SPONGE)) {
                        if (above.getType().equals(Material.GOLD_PLATE)) {
                            corners.add(b.getLocation());
                            UtilWorld.quickChangeBlock(world, b.getLocation(), 0, 0);
                            UtilWorld.quickChangeBlock(world, b.getRelative(BlockFace.UP).getLocation(), 0, 0);
                            Bukkit.broadcastMessage("Found corner at " + UtilLocation.toString(b.getLocation(), false));
                        }
                    }
                }
                y = 0;
            }
            z = -size;
        }
        ready = false; //don't call again

        if (corners.size() != 2) {
            corners.add(new Location(world, -255, 0, -255));
            corners.add(new Location(world, 255, 256, 255));
            Bukkit.broadcastMessage(C.darkRed + "Need 2 corner blocks !!! defaulting to -255 to 255 corners");
        }

        int minX = Math.min(corners.get(0).getBlockX(), corners.get(1).getBlockX());
        int minY = Math.min(corners.get(0).getBlockY(), corners.get(1).getBlockY());
        int minZ = Math.min(corners.get(0).getBlockZ(), corners.get(1).getBlockZ());

        int maxX = Math.max(corners.get(0).getBlockX(), corners.get(1).getBlockX());
        int maxY = Math.max(corners.get(0).getBlockY(), corners.get(1).getBlockY());
        int maxZ = Math.max(corners.get(0).getBlockZ(), corners.get(1).getBlockZ());


        List<String> lines = new ArrayList<>(Arrays.asList(
                "NAME: " + world.getName(),
                "",
                "MinX: " + minX,
                "MinY: " + minY,
                "MinZ: " + minZ,
                "",
                "MaxX: " + maxX,
                "MaxY: " + maxY,
                "MaxZ: " + maxZ,
                ""
        ));

        for (DyeColor c : spawnLocs.keySet()) {
            lines.add("TEAM: " + c.name());
            lines.add("TEAM_SPAWNS: " + UtilLocation.toString(spawnLocs.get(c), false));
        }

        lines.add("");

        for (DyeColor c : dataLocs.keySet()) {
            lines.add("DATA: " + c.name());
            lines.add("DATA_LOCS: " + UtilLocation.toString(dataLocs.get(c), false));
        }
        lines.add("");

        for (String s : signLocs.keySet()) {
            lines.add("CUSTOM: " + s);
            lines.add("CUSTOM_LOCS: " + UtilLocation.toString(signLocs.get(s), false));
        }

        try {
            FileUtils.writeLines(new File(mapLoc, "data.yml"), lines);
        } catch (IOException e) {
            e.printStackTrace();
        }


        UtilWorld.saveWorld(world.getName(), gametype, bool -> {
            Bukkit.broadcastMessage("Successfully saved live map.");

            UtilServer.runTaskLater(() -> {
                String worldName = world.getName();
                UtilWorld.unloadBuildWorld(worldName, gametype, o -> Bukkit.broadcastMessage("Saved world " + worldName));

                UtilServer.unregisterListener(this);
            }, 20L);
        });

        return true;
    }
}
