package com.infinity.parser;

import com.google.common.collect.Lists;
import me.adamtanana.core.command.Command;
import me.adamtanana.core.player.Rank;
import me.adamtanana.core.util.C;
import me.adamtanana.core.util.UtilWorld;
import org.bukkit.World;
import org.bukkit.entity.Player;

import java.util.List;

public class FinishCommand extends Command {

    public FinishCommand() {
        super("finish", Lists.newArrayList(), 0, Rank.BUILDER);
    }

    @Override
    public void execute(Player player, List<String> list) {
        World world = player.getWorld();
        if (world.getName().equals("world")) return;

        String type = Parser.getGametypes().remove(player.getWorld());
        UtilWorld.saveBuildWorld(player.getWorld().getName(), type);
        UtilWorld.unloadBuildWorld(player.getWorld().getName(), type, (o) -> player.sendMessage(C.darkAqua + "World unloaded and saved!"));
    }
}

