package com.infinity.parser;

import com.google.common.collect.Lists;
import me.adamtanana.core.command.Command;
import me.adamtanana.core.player.Rank;
import me.adamtanana.core.util.C;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;

import java.util.List;

public class ParseCommand  extends Command {

    public ParseCommand() {
        super("parse", Lists.newArrayList(), 1, Rank.BUILDER);
    }

    @Override
    public void execute(Player player, List<String> list) {
        World world = player.getWorld();
        if(world.getName().equals("world")) return;

        int count;
        try {
            count = Integer.valueOf(list.get(0));
        } catch (Exception e) {
            player.sendMessage(list.get(0) + " is not a number");
            return;
        }

        if(!Parser.setCurrentParse(new Parse(world, count))) {
            player.sendMessage(C.darkRed + "A parse has already been started!!!");
        }else {
            world.getPlayers().forEach(p -> p.teleport(Bukkit.getWorld("world").getSpawnLocation()));
        }
    }
}
