package com.infinity.parser;

import com.google.common.collect.Maps;
import me.adamtanana.core.Core;
import me.adamtanana.core.command.CommandHandler;
import me.adamtanana.core.event.ProfileLoadedEvent;
import me.adamtanana.core.event.UpdateEvent;
import me.adamtanana.core.event.UpdateType;
import me.adamtanana.core.player.Rank;
import me.adamtanana.core.redis.ServerType;
import me.adamtanana.core.util.C;
import me.adamtanana.core.util.UtilWorld;
import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.*;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

import java.util.HashMap;

public class Parser extends Core {

    private static HashMap<World, String> gameType = Maps.newHashMap();
    private static Parse currentParse = null;

    public static boolean setCurrentParse(Parse currentParse) {
        if(Parser.currentParse != null) return false;
        Parser.currentParse = currentParse;
        return true;
    }

    @Override
    public void enable() {
        CommandHandler.register(new StartCommand());
        CommandHandler.register(new FinishCommand());
        CommandHandler.register(new ParseCommand());
        CommandHandler.register(new ListCommand());
        CommandHandler.register(new ContinueCommand());

    }

    @Override
    public ServerType getServerType() {
        return ServerType.PARSER;
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent event) {
        if(!event.getPlayer().getWorld().getName().equals("world") && event.getPlayer().getWorld().getPlayers().size() < 1) {
            if(getGametypes().containsKey(event.getPlayer().getWorld())) return;
            Player player = event.getPlayer();
            UtilWorld.unloadBuildWorld(player.getWorld().getName(), Parser.getGametypes().remove(player.getWorld()) ,(o) -> player.sendMessage(C.darkAqua + "World unloaded and saved!"));
        }
    }


    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        event.getPlayer().teleport(Bukkit.getWorld("world").getSpawnLocation());
        event.getPlayer().setGameMode(GameMode.CREATIVE);
    }

    @EventHandler
    public void onLoad(ProfileLoadedEvent e) {
        if(e.getGp().getRank().has(Rank.BUILDER, Rank.HELPER, Rank.MOD)) e.getPlayer().setOp(true);
    }

    public static HashMap<World, String> getGametypes() {
        return gameType;
    }

    @EventHandler
    public void update(UpdateEvent e) {
        if(e.getType() != UpdateType.TICK) return;
        if(currentParse == null) return;
        if(currentParse.update()) {
            Bukkit.broadcastMessage(C.darkAqua + "Parse complete!");
            currentParse = null;
        }
    }

    @EventHandler
    public void onPhysics(BlockPhysicsEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onWeather(WeatherChangeEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onGrass(BlockSpreadEvent e) {
        if(e.getBlock().getRelative(BlockFace.UP).getType() == Material.WOOL) e.setCancelled(true);
    }

    @EventHandler
    public void disableCreatures(EntitySpawnEvent event)
    {
        if (event.getEntityType() == EntityType.DROPPED_ITEM || event.getEntity() instanceof LivingEntity)
            event.setCancelled(true);
    }

    @EventHandler
    public void disableBurn(BlockBurnEvent event)
    {
        event.setCancelled(true);
    }

    @EventHandler
    public void disableIgnite(BlockIgniteEvent event)
    {
        if (event.getCause() == BlockIgniteEvent.IgniteCause.LAVA || event.getCause() == BlockIgniteEvent.IgniteCause.SPREAD)
            event.setCancelled(true);
    }

    @EventHandler
    public void disableFire(BlockSpreadEvent event)
    {
        event.setCancelled(true);
    }

    @EventHandler
    public void disableFade(BlockFadeEvent event)
    {
        event.setCancelled(true);
    }

    @EventHandler
    public void disableDecay(LeavesDecayEvent event)
    {
        event.setCancelled(true);
    }

    @EventHandler
    public void disableIceForm(BlockFormEvent event)
    {
        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void FlySpeed(PlayerCommandPreprocessEvent event)
    {
        if (!event.getMessage().toLowerCase().startsWith("/speed"))
            return;

        Player player = event.getPlayer();

        String[] tokens = event.getMessage().split(" ");

        if (tokens.length != 2)
        {
            return;
        }

        event.setCancelled(true);

        try
        {
            float speed = Float.parseFloat(tokens[1]);

            player.setFlySpeed(speed);

            player.sendMessage("Speed set to " + speed);
        }
        catch (Exception e)
        {
            player.sendMessage("Unknown speed");
        }
    }


}
