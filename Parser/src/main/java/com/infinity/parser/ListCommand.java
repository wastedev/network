package com.infinity.parser;

import me.adamtanana.core.command.Command;
import me.adamtanana.core.engine.GameType;
import me.adamtanana.core.player.Rank;
import me.adamtanana.core.util.C;
import me.adamtanana.core.util.UtilText;
import me.adamtanana.core.util.UtilWorld;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class ListCommand extends Command {
    public ListCommand() {
        super("maps", Arrays.asList("list"), 0, Rank.BUILDER);
    }

    @Override
    public void execute(Player player, List<String> args) {
        player.sendMessage(C.red + "You must use a valid game type. These include: ");
        StringBuilder msg = new StringBuilder("");
        for (GameType gameType : GameType.values()) {
            msg.append(gameType.name() + ", ");
        }
        msg.setLength(msg.length() - 2);
        player.sendMessage(C.darkAqua + msg.toString());
        player.sendMessage(C.aqua+ "Maps: ");
        player.sendMessage(" ");

        HashMap<String, List<String>> maps = UtilWorld.getAllBuildMaps();
        //Maps
        for(String gameType : maps.keySet()) {
            player.sendMessage(gameType + C.darkAqua + UtilText.join(maps.get(gameType), ". "));
        }

    }
}
