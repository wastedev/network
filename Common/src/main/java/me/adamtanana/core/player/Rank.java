package me.adamtanana.core.player;

import lombok.AllArgsConstructor;
import lombok.Getter;
import me.adamtanana.core.util.C;

@Getter
@AllArgsConstructor
public enum Rank {
    MEMBER(C.reset, "", "K"),
    PRO(C.reset, "Pro", "J"),
    ULTIMATE(C.lightPurple, "Ultimate", "I"),
    GOD(C.darkPurple, "God", "H"),
    YOUTUBE(C.red, "Youtube", "G"),
    BUILDER(C.reset, "Builder", "F"),
    HELPER(C.yellow, "Helper", "E"),
    MOD(C.gold, "Mod", "D"),
    ADMIN(C.gold, "Admin", "C"),
    DEVELOPER(C.gold, "Dev", "B"),
    OWNER(C.darkRed, "Owner", "A");

    private String color, name = "";
    private String key;

    public String getName(boolean caps, boolean bold, boolean hasColor) {
        return (hasColor ? color : "") + (bold ? C.bold : "") + (caps ? name.toUpperCase() : name) + C.reset;
    }

    public boolean has(Rank rank, Rank... blacklist) {
        for(Rank r : blacklist) if(r == rank) return false;
        return this.compareTo(rank) >= 0;
    }


}
