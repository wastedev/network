package me.adamtanana.core.player;

import lombok.Getter;
import me.adamtanana.core.database.Model;
import me.adamtanana.core.database.Storable;
import me.adamtanana.core.util.C;

import java.util.Date;
import java.util.UUID;

@Model
public class BanEntry {
    @Storable
    private String banReason;

    @Storable
    @Getter
    private long bannedOn;

    @Storable
    @Getter
    private String ip = "";

    @Storable
    private long dateDiff;

    @Storable
    @Getter
    private String staff;

    @Getter
    private String kickReason = "";

    public BanEntry() {
        if (isPermanent()) {
            this.kickReason = "&4Banned: &c" + banReason + "\n&eYou are &4Permanently&e banned!";
        } else if (isKick()) {
            this.kickReason = "&4Kicked: &c" + banReason;
        } else {
            this.kickReason = "&4Banned: &c" + banReason + "\n&eYou are banned until &4" + getExpiryDate() + "&e!";
        }
    }

    public BanEntry(String banner, String ip, String banReason, long bannedOn, long dateDiff) {
        this.bannedOn = bannedOn;
        this.staff = banner;
        this.ip = ip;
        this.dateDiff = dateDiff;

        if (isPermanent()) {
            this.kickReason = "&4Banned: &c" + banReason + "\n&eYou are &4Permanently&e banned!";
        } else if (isKick()) {
            this.kickReason = "&4Kicked: &c" + banReason;
        } else {
            this.kickReason = "&4Banned: &c" + banReason + "\n&eYou are banned until &4" + getExpiryDate() + "&e!";
        }
    }

    public String getBanReason() {
        return C.replaceColors(banReason);
    }

    public String getExpiryDate() {
        return new Date(bannedOn + (dateDiff * 1000)).toString();
    }

    public boolean expired() {
        if (isPermanent()) return false;
        return System.currentTimeMillis() - bannedOn >= dateDiff;
    }

    public boolean isKick() {
        return dateDiff == 0;
    }

    public boolean isPermanent() {
        return dateDiff < 0;
    }

}
