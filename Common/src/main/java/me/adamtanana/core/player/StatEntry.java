package me.adamtanana.core.player;

import lombok.Getter;
import lombok.Setter;
import me.adamtanana.core.database.Model;
import me.adamtanana.core.database.Storable;

import java.util.UUID;

@Model
public class StatEntry {
    @Storable
    @Getter
    private String statName;

    @Storable
    @Getter
    private String gameType;

    @Storable
    @Getter
    @Setter
    private long data;

    public StatEntry() {
    }

    public StatEntry(String gameType, String statName, long data) {
        this.statName = statName;
        this.gameType = gameType;
        this.data = data;
    }

    @Override
    public String toString() {
        return "Stat: " + statName + ", game: " + gameType + ", data: " + data + "}";
    }


}

