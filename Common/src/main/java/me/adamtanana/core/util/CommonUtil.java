package me.adamtanana.core.util;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Lists;
import com.google.gson.*;
import me.adamtanana.core.player.BanEntry;
import me.adamtanana.core.player.GlobalPlayer;

import javax.net.ssl.HttpsURLConnection;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Constructor;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class CommonUtil {
    private static final String MOJANG_NAMES_URL = "https://api.mojang.com/user/profiles/%s/names";
    private static final JsonParser JSON_PARSER = new JsonParser();

    private static final LoadingCache<String, UUID> nameUuidCache = CacheBuilder.newBuilder().expireAfterAccess(30L, TimeUnit.MINUTES).maximumSize(100L).build(new CacheLoader<String, UUID>() {
        @Override
        public UUID load(String name) throws Exception {
            HttpsURLConnection connection = (HttpsURLConnection) new URL("https://api.mojang.com/profiles/minecraft").openConnection();
            connection.setUseCaches(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(true);
            connection.setDoInput(true);
            JsonArray arr = new JsonArray();
            arr.add(new JsonPrimitive(name));
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
            out.write(arr.toString());
            out.flush();
            JsonArray json = (JsonArray) JSON_PARSER.parse(new InputStreamReader(connection.getInputStream()));
            if(json.size() == 0) return UUID.nameUUIDFromBytes("null".getBytes());
            String uuid = json.get(0).getAsJsonObject().get("id").getAsString();
            out.close();
            return parseUUID(uuid);
        }
    });
    private static final LoadingCache<UUID, GlobalPlayer> uuidPlayerCache = CacheBuilder.newBuilder().expireAfterAccess(1L, TimeUnit.HOURS).maximumSize(100L).build(new CacheLoader<UUID, GlobalPlayer>() {
        @Override
        public GlobalPlayer load(UUID uuid) throws Exception {
            GlobalPlayer player = new GlobalPlayer(uuid);
            return player;
        }
    });

    /**
     * Parse uuid from string
     *
     * @param uuidStr
     * @return
     */
    public static UUID parseUUID(String uuidStr) {
        //Split uuid in to 5 components
        String[] uuidComponents = new String[]{
                uuidStr.substring(0, 8),
                uuidStr.substring(8, 12),
                uuidStr.substring(12, 16),
                uuidStr.substring(16, 20),
                uuidStr.substring(20, uuidStr.length())
        };

        //Combine components with a dash
        StringBuilder builder = new StringBuilder();
        for (String component : uuidComponents) {
            builder.append(component).append('-');
        }

        //Correct uuid length, remove last dash
        builder.setLength(builder.length() - 1);
        return UUID.fromString(builder.toString());
    }

    /**
     * Force fetch the player's UUID from mojang(cached)
     *
     * @param name Name of the player
     * @return UUID of the player
     */
    public static UUID forceFetchUUID(String name) {
        try {
            UUID id = nameUuidCache.get(name);
            if(id.equals(UUID.nameUUIDFromBytes("null".getBytes()))) return null;
            return nameUuidCache.get(name);
        } catch (ExecutionException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Returns the player's current name.
     * Cached for 30 minutes.
     *
     * @param uuid Unique user id of player
     * @return Current name of player, null if failed to retrieve.
     */
    public static String getNameByUUID(UUID uuid) {
        try {
            GlobalPlayer player = uuidPlayerCache.get(uuid);
            return player.getLastName();
        } catch (ExecutionException e) {
            return null;
        }
    }

    /**
     * Delete the cached values for a player
     *
     * @param uuid Player's UUID
     */
    public static void invalidateGlobalPlayer(UUID uuid) {
        uuidPlayerCache.invalidate(uuid);
    }

    /**
     * Get global player info by player uuid.
     *
     * @param uuid Unique user id of player.
     * @return Global player info
     */
    public static GlobalPlayer getGlobalPlayer(UUID uuid) {
        if(uuid == null) return null;
        try {
            return uuidPlayerCache.get(uuid);
        } catch (ExecutionException e) {
            return null;
        }
    }

    /**
     * My maths stuff
     **/
    public static byte degreeToByte(float f) {
        return (byte) (int) (f * 256f / 360f);
    }

    public static float degreeToRadian(float f) {
        return (float) (((f + 90) * Math.PI) / 180);
    }

    public static float radianToDegree(float f) {
        return f * 57.2957795f;
    }

    public static Object instance(Class<?> clazz, Object... args) {
        try {
            for (Constructor<?> constructor : clazz.getDeclaredConstructors()) {
                if (constructor.getParameterTypes().length == args.length) {
                    boolean typeMismatch = false;
                    for (int i = 0; i < args.length; i++) {
                        if (!constructor.getParameterTypes()[i].isInstance(args[i])) {
                            typeMismatch = false;
                        }
                    }

                    if (!typeMismatch) {
                        if (!constructor.isAccessible()) {
                            constructor.setAccessible(true);
                        }
                        try {
                            return constructor.newInstance(args);
                        } catch (Exception e) {
                            try {
                                return clazz.newInstance();
                            } catch (Exception e1) {
                                throw new IllegalArgumentException("Invalid amount of parameters when constructing class!", e1);
                            }
                        }
                    }
                }
            }

            //Attempt to use 0 args
            try {
                return clazz.newInstance();
            } catch (Exception e) {
                throw new IllegalArgumentException("Invalid amount of parameters when constructing class!", e);
            }
        } catch (Exception e) {
            throw new IllegalArgumentException("Class constructing failed", e);
        }
    }

    public static List<GlobalPlayer> getAllCachedPlayers() {
        return new ArrayList<>(uuidPlayerCache.asMap().values());
    }
}
