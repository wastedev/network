package me.adamtanana.core.redis.message;

import lombok.Getter;
import me.adamtanana.core.redis.Message;
import me.adamtanana.core.redis.ServerType;

import java.net.InetAddress;
import java.net.InetSocketAddress;

public class GetServerName implements Message {

    private @Getter String name;
    private @Getter ServerType serverType;
    private @Getter String address;
    private @Getter int port;

    public GetServerName() {}

    public GetServerName(String name) {
        this.name = name;
    }

    public GetServerName(ServerType type, String address, int port) {
        this.serverType = type;
        this.address = address;
        this.port = port;
    }
}
