package me.adamtanana.core.redis.message;

import lombok.AllArgsConstructor;
import lombok.Getter;
import me.adamtanana.core.redis.Message;
import me.adamtanana.core.redis.ServerType;

import java.util.UUID;

@AllArgsConstructor
@Getter
public class SendGametypeMessage implements Message {
    private String player;
    private ServerType to;

    public SendGametypeMessage() {

    }

}
