package me.adamtanana.core.redis.message;

import lombok.Getter;
import me.adamtanana.core.redis.Message;

public class KillServerMessage implements Message {

    private @Getter
    String name;

    public KillServerMessage() {}

    public KillServerMessage(String name) {
        this.name = name;
    }
}
