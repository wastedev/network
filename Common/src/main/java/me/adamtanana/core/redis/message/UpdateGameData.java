package me.adamtanana.core.redis.message;

import lombok.AllArgsConstructor;
import lombok.Getter;
import me.adamtanana.core.redis.Message;

@AllArgsConstructor
@Getter
public class UpdateGameData implements Message {

    private int maxPlayers;
    private boolean started;

    public UpdateGameData() {
    }
}
