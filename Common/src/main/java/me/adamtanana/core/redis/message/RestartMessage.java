package me.adamtanana.core.redis.message;

import lombok.Getter;
import me.adamtanana.core.redis.Message;

public class RestartMessage implements Message {
    private @Getter String name;

    public RestartMessage() {}

    public RestartMessage(String name) {
        this.name = name;
    }
}
