package com.infinity.bungee;

import com.google.common.collect.Maps;

import java.util.HashMap;

public class GameServerData {
    private HashMap<String, GameData> data = Maps.newHashMap();

    public HashMap<String, GameData> getData() {
        return data;
    }

    public static class GameData {
        private int maxPlayers;
        private boolean started;

        public GameData(boolean started, int maxPlayers) {
            this.started = started;
            this.maxPlayers = maxPlayers;
        }

        public int getMaxPlayers() {
            return maxPlayers;
        }

        public boolean hasStarted() {
            return started;
        }
    }
}
