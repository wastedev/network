package com.infinity.bungee;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import me.adamtanana.core.redis.MessageListener;
import me.adamtanana.core.redis.ServerType;
import me.adamtanana.core.redis.message.HeartbeatCallMessage;
import me.adamtanana.core.redis.message.HeartbeatReplyMessage;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.plugin.Listener;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class Heartbeat implements MessageListener<HeartbeatCallMessage> {
    private HashMap<String, Long> serverLastCall = Maps.newHashMap();
    private GameServerData gameServerData;

    public Heartbeat(Bungee instance, GameServerData gameServerData) {
        this.gameServerData = gameServerData;
        ProxyServer.getInstance().getScheduler().schedule(instance, () -> {
            update();
        }, 5L, 1L, TimeUnit.SECONDS);
    }

    public void update() {
        List<String> toRemove = Lists.newArrayList();
        for(String server : serverLastCall.keySet()) {
            long lastCall = serverLastCall.get(server);
            if(System.currentTimeMillis() - lastCall >= 5000) { //5 seconds before last cal
                System.out.println(server + " hasn't responded within 5 seconds, removing it!");
                toRemove.add(server);
            } else if(!ProxyServer.getInstance().getServers().containsKey(server)) { //Not alive anymore
                toRemove.add(server);
                System.out.println(server + " isn't in the servers list anymore. Possibly unregistered?");
            }
        }

        toRemove.forEach(server -> {
            serverLastCall.remove(server);
            gameServerData.getData().remove(server);
            ProxyServer.getInstance().getServers().remove(server);
        });
    }

    @Override
    public void onReceive(String sender, HeartbeatCallMessage msg) {
        serverLastCall.put(sender, System.currentTimeMillis());
        Bungee.getJedis().sendMessage(new HeartbeatReplyMessage(), sender);
    }

    public HashMap<String, Long> getServerLastCall() {
        return serverLastCall;
    }
}
