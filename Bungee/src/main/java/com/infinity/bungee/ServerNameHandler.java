package com.infinity.bungee;

import com.google.common.collect.Lists;
import me.adamtanana.core.redis.MessageListener;
import me.adamtanana.core.redis.ServerType;
import me.adamtanana.core.redis.message.GetServerName;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;

import java.net.InetSocketAddress;
import java.util.List;
import java.util.regex.Pattern;

public class ServerNameHandler implements MessageListener<GetServerName> {

    @Override
    public void onReceive(String sender, GetServerName msg) {
        ServerType type = msg.getServerType();
        String newName = getNams(type);


        ServerInfo info = ProxyServer.getInstance().constructServerInfo(newName, new InetSocketAddress(msg.getAddress(), msg.getPort()), "MOTD", false);
        ProxyServer.getInstance().getServers().put(info.getName(), info);
        System.out.println("Registered: " + newName + " instead of " + sender);
        Bungee.getJedis().sendMessage(new GetServerName(newName), sender);

    }

    private String getNams(ServerType type) {
        List<Integer> numbers = Lists.newArrayList();

        ProxyServer.getInstance().getServers().forEach((name, info) -> {
            if(name.startsWith(type.name())) {
                numbers.add(Integer.valueOf(name.split(Pattern.quote("-"))[1]));
            }
        });
        for(int i = 1; i < 500; i++) {
            if(!numbers.contains(i)) return type.name() + "-" + i; //HUB-1
        }

        return "";
    }


}
