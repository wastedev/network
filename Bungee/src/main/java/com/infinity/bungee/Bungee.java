package com.infinity.bungee;

import com.google.common.collect.Lists;
import me.adamtanana.core.redis.JedisServer;
import me.adamtanana.core.redis.ServerType;
import me.adamtanana.core.redis.message.*;
import me.adamtanana.core.util.C;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.*;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.event.EventHandler;

import java.lang.reflect.Proxy;
import java.sql.Timestamp;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Bungee extends Plugin implements Listener{

    private static JedisServer jedis;
    private List<String> serversToRestart = Lists.newArrayList();
    private GameServerData gameServerData;

    @Override
    public void onEnable() {
        jedis = new JedisServer("bungee");
        gameServerData = new GameServerData();
        Heartbeat hb = new Heartbeat(this, gameServerData);

        jedis.registerListener(GetServerName.class, new ServerNameHandler());

        jedis.registerListener(KillServerMessage.class, (sender, msg) -> {
            System.out.println("Unregistered: " + msg.getName());
            serversToRestart.remove(msg.getName());
            ProxyServer.getInstance().getServers().remove(msg.getName());
            hb.getServerLastCall().put(msg.getName(), System.currentTimeMillis());
        });

        jedis.registerListener(RestartMessage.class, (sender, msg) -> {
            if (msg.getName().equalsIgnoreCase("all")) {
                ProxyServer.getInstance().getPlayers().forEach(p -> p
                                .disconnect(new TextComponent(C.red + "Server is restarting for an update!\n" + C.reset + "You can join again soon!")));
            } else {
                ServerInfo random = getRandomServer(ServerType.HUB, msg.getName());
                if (random == null) return;
                ProxyServer.getInstance().getServerInfo(msg.getName()).getPlayers().stream()
                        .filter(p -> p.isConnected() && p != null).forEach(p -> p.connect(random));
                serversToRestart.add(msg.getName());
            }
        });

        jedis.registerListener(UpdateGameData.class, (sender, msg) -> {
            gameServerData.getData().put(sender, new GameServerData.GameData(msg.isStarted(), msg.getMaxPlayers()));
        });

        jedis.registerListener(SendGametypeMessage.class, (sender, msg) -> {
            ProxyServer.getInstance().getPlayer(msg.getPlayer()).connect(getOpenServer(msg.getTo()));
        });

        ProxyServer.getInstance().getPluginManager().registerListener(this, this);
        ProxyServer.getInstance().getServers().remove("lobby");
        JedisServer.registerListener(HeartbeatCallMessage.class, hb);
    }

    public static JedisServer getJedis() { return jedis; }


    public static ServerInfo getRandomServer(ServerType type, String blacklist) {
        for(ServerInfo info : ProxyServer.getInstance().getServers().values()) {
            try {
                System.out.println(info.getName() + " found");
                if (info.getName().equalsIgnoreCase(blacklist)) continue;
                if (info.getName().startsWith(type.name())) return info;
            } catch (Exception e) {
            }
        }

        return null;
    }

    public ServerInfo getOpenServer(ServerType type) {
        List<ServerInfo> sorted = ProxyServer.getInstance().getServers().values().stream()
                .filter(serverInfo -> serverInfo.getName().startsWith(type.name()))
                .filter(serverInfo -> !gameServerData.getData()
                        .getOrDefault(serverInfo.getName(), new GameServerData.GameData(false, 1)).hasStarted())
                .sorted((o1, o2) -> o1.getPlayers().size() - o2.getPlayers().size()).collect(Collectors.toList());
        return sorted.size() > 0 ? sorted.get(0) : null;
    }

    @EventHandler
    public void switchServer(ServerConnectEvent e) {
        if(serversToRestart.contains(e.getTarget().getName())) {
            e.setCancelled(true);
            e.getPlayer().sendMessage(C.gray + "That server is currently restarting");
            return;
        }
    }

    @EventHandler
    public void switchServer(ServerSwitchEvent e) {
        e.getPlayer().sendMessage(C.gray + "You have been sent to " + e.getPlayer().getServer().getInfo().getName());
    }

    @EventHandler
    public void onLogin(ProxyJoinEvent event) {
        ProxiedPlayer p = event.getPlayer();
        p.sendMessage(C.yellow + "Welcome to the Tropilus network, " + p.getName() + "!");
        p.sendTitle(ProxyServer.getInstance().createTitle().title(new TextComponent(C.aqua + "Welcome to Tropilus!")).subTitle(new TextComponent("We're currently in beta, please excuse any bugs!")));
        event.setServer(getRandomServer(ServerType.HUB, ""));
    }

    @EventHandler
    public void onPing(ProxyPingEvent event) {
        ServerPing ping = event.getResponse();

//        2592000000L
        long future = 1466878919505L + 2592000000L, now = System.currentTimeMillis(), difference = future - now;
        Timestamp time = new Timestamp(difference);

        String days = C.gold + time.getDay() + C.gray, mins = C.gold + String.valueOf(time.getMinutes()) + C.gray, secs = C.gold + String.valueOf(time.getSeconds()) + C.gray;
        if(days.toCharArray().length == 1) days = C.gold + "0" + days + C.gray;
        if(mins.toCharArray().length == 1) mins = C.gold + "0" + mins + C.gray;
        if(secs.toCharArray().length == 1) secs = C.gold + "0" + secs + C.gray;

        ping.setDescription(C.white+ "[" + C.aqua + "BETA" + C.white + "] " + C.red + "Tropilus Network" + "\n" + C.yellow + "It's almost here, " + days + ":" + mins + ":" + secs + C.white + "   <- only a test");
    }

}
