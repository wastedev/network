package com.infinity.arcade.escape;

import me.adamtanana.core.engine.Game;
import me.adamtanana.core.engine.GameOptions;
import me.adamtanana.core.engine.GameSetting;
import me.adamtanana.core.engine.GameType;
import me.adamtanana.core.engine.damage.DamageHandler;
import me.adamtanana.core.engine.player.GamePlayer;
import me.adamtanana.core.engine.player.PlayerState;
import me.adamtanana.core.engine.team.Team;
import me.adamtanana.core.engine.timer.GameTimer;
import me.adamtanana.core.player.StatEntry;
import me.adamtanana.core.util.C;
import me.adamtanana.core.util.CommonUtil;
import me.adamtanana.core.util.UtilServer;
import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;

import java.util.List;

public class Escape extends Game {

    public Escape() {
        super(GameType.ESCAPE, new int[]{ 2, 16 }, new String[]{"line1", "line2"});
        setGameTimer(new GameTimer(this, 0, 10, 0));
    }

    @Override
    public void start() {
        final int[] counter = {0}; // Final bla bla
        getLivePlayers().forEach(p -> p.setMetadata("block", new FixedMetadataValue(UtilServer.getPlugin(), counter[0] > 16 ? counter[0] = 0 : counter[0]++)));
    }

    @Override
    public void finish() {
        for(GamePlayer p : getGamePlayers()) {
            addPoints(p, "Participation", 10);
            CommonUtil.getGlobalPlayer(p.getUuid()).addStat(new StatEntry(getGameType().name(), "Games Played", 1));
        }

        if(getWinners()[0] != null && Bukkit.getPlayer(getWinners()[0]) != null) {
            Player p = Bukkit.getPlayer(getWinners()[0]);
            addPoints(p.getUniqueId(), "1st Place", 100);
            CommonUtil.getGlobalPlayer(p.getUniqueId()).addStat(new StatEntry(getGameType().name(), "Wins", 1));
        }
        if(getWinners()[1] != null && Bukkit.getPlayer(getWinners()[1]) != null) addPoints(Bukkit.getPlayer(getWinners()[1]).getUniqueId(), "2nd Place", 75);
        if(getWinners()[2] != null  && Bukkit.getPlayer(getWinners()[2]) != null) addPoints(Bukkit.getPlayer(getWinners()[2]).getUniqueId(), "3rd Place", 50);
    }

    @Override
    public void teleport(List<Team> teams) {
        teams.forEach(team -> {
            int counter = 0;
            if(getGameWorld().getTeamSpawns().containsKey(team.getTeamColor())) {
                List<Location> locs = getGameWorld().getTeamSpawns().get(team.getTeamColor());
                for (GamePlayer gamePlayer : team.getGamePlayers()) {
                    if(gamePlayer.getPlayer() == null) continue;
                    if(counter >= locs.size()) counter=0;
                    gamePlayer.getPlayer().teleport(locs.get(counter++));
                }
            } else {
                Location loc = getGameWorld().getTeamSpawns().get(DyeColor.BLACK).get(0);
                for (GamePlayer gamePlayer : team.getGamePlayers()) {
                    if(gamePlayer.getPlayer() == null) continue;
                    gamePlayer.getPlayer().teleport(loc);
                    gamePlayer.setPlayerState(PlayerState.SPECTATING);
                    gamePlayer.getPlayer().sendMessage(C.darkAqua + "Couldn't find spawn point, You're now a spectator.");
                }
            }
        });
    }

    @Override
    public void register() {
        GameOptions options = new GameOptions();
        options.setWorld(getGameWorld().getWorld().getName());
        options.setMaxHealth(6);

        enableModules(options, new EscapePhysics(this));
    }

    @Override
    public GameSetting getGameSetting() {
        return GameSetting.NORMAL;
    }
}
