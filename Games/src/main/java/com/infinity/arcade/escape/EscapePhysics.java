package com.infinity.arcade.escape;

import me.adamtanana.core.Module;
import me.adamtanana.core.engine.damage.DamageEvent;
import me.adamtanana.core.engine.damage.DamageHandler;
import me.adamtanana.core.event.UpdateEvent;
import me.adamtanana.core.event.UpdateType;
import me.adamtanana.core.player.StatEntry;
import me.adamtanana.core.util.CommonUtil;
import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.event.entity.PlayerDeathEvent;

import java.util.List;
import java.util.stream.Collectors;

public class EscapePhysics extends Module {

    private Material fallingBlockMaterial;
    private Escape escape;
    private int maxFallHeight = 80, defaultHeight = 15;
    private double fallSpeed;

    public EscapePhysics(Escape escape) {
        this.escape = escape;
        this.fallingBlockMaterial = Material.STAINED_CLAY;
    }

    @Override
    public void enable() {
        maxFallHeight = escape.getGameWorld().getDataPoints().get(DyeColor.RED).get(0).getBlockY()+5;
    }

    @EventHandler
    public void spawnFallingBlocks(UpdateEvent event) {
        if(event.getType() != UpdateType.FASTER) return;
        if(!escape.isLive()) return;
        escape.getLivePlayers().forEach(player -> {
            Location playerLoc = player.getLocation();
            int height = (playerLoc.getBlockY()+defaultHeight > maxFallHeight ? maxFallHeight : playerLoc.getBlockY()+defaultHeight);
            FallingBlock block = player.getWorld().spawnFallingBlock(new Location(player.getWorld(), playerLoc.getBlockX(), height, playerLoc.getBlockZ()), fallingBlockMaterial,  player.getMetadata("block").get(0).asByte());
            block.setVelocity(block.getVelocity().setY(-getFallingSpeed()));
            CommonUtil.getGlobalPlayer(player.getUniqueId()).addStat(new StatEntry(escape.getGameType().name(), "Blocks Dropped", 1));
        });
    }

    @EventHandler
    public void onItemSpawn(ItemSpawnEvent event) {
        if(event.getEntity().getItemStack().getType() == fallingBlockMaterial) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onFallingBlockLand(EntityChangeBlockEvent event) {
        if(!(event.getEntity() instanceof FallingBlock)) return;
        if(!escape.isLive()) return;
        List<Player> playersAffected = event.getEntity().getNearbyEntities(0.5, 0.5, 0.5).stream().filter(p -> p instanceof Player).map(e -> (Player) e).collect(Collectors.toList());
        if(!playersAffected.isEmpty()) {
            event.setCancelled(true);
            playersAffected.forEach(player -> escape.getModule(DamageHandler.class).damagePlayer(player, 2, "Falling Block"));
        }
    }

    @EventHandler
    public void onFallingBlockDamage(DamageEvent event) {
        if(event.getCause() == EntityDamageEvent.DamageCause.FALLING_BLOCK) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onDeath(PlayerDeathEvent event) {
        Player player = event.getEntity();

        CommonUtil.getGlobalPlayer(player.getUniqueId()).addStat(new StatEntry( escape.getGameType().name(), "Deaths", 1));
    }

    public double getFallingSpeed() {
        switch (escape.getGameSetting()) {
            case NORMAL: fallSpeed = 0.8; break;
            case CRAZY: fallSpeed = 1; break;
            case MEGA: fallSpeed = 0.8; break;
            case TOURNAMENT: fallSpeed = 0.8; break;
            case EVENT: fallSpeed = 0.8; break;
        }

        return fallSpeed;
    }
}
