package com.infinity.arcade;

import com.infinity.arcade.escape.Escape;
import me.adamtanana.core.Core;
import me.adamtanana.core.engine.EngineHandler;
import me.adamtanana.core.redis.ServerType;

public class Arcade extends Core {
    @Override
    protected void enable() {
        enableModules(new EngineHandler(new Escape()));
    }

    @Override
    public ServerType getServerType() {
        return ServerType.ESCAPE; //TODO LATER, if multiple games make it GAME server, else make it one server type
    }

}
