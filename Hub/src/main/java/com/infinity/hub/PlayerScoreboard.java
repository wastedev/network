package com.infinity.hub;

import me.adamtanana.core.Module;
import me.adamtanana.core.event.ProfileLoadedEvent;
import me.adamtanana.core.player.GlobalPlayer;
import me.adamtanana.core.scoreboard.common.EntryBuilder;
import me.adamtanana.core.scoreboard.common.animate.HighlightedString;
import me.adamtanana.core.scoreboard.type.ByteScoreboard;
import me.adamtanana.core.scoreboard.type.Entry;
import me.adamtanana.core.scoreboard.type.ScoreboardHandler;
import me.adamtanana.core.util.C;
import me.adamtanana.core.util.CommonUtil;
import me.adamtanana.core.util.UtilEntity;
import me.adamtanana.core.util.UtilServer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.List;

public class PlayerScoreboard extends Module {

    @EventHandler
    public void addPlayer(ProfileLoadedEvent event) {
        GlobalPlayer globalPlayer = event.getGp();

        new ByteScoreboard(Bukkit.getPlayer(event.getGp().getUniqueUserId())).setHandler(new ScoreboardHandler() {

            HighlightedString title = new HighlightedString("Tropilus Network", "&f&l", "&c&l");

            @Override
            public String getTitle(Player player) {
                return title.next();
            }

            @Override
            public List<Entry> getEntries(Player player) {

                return new EntryBuilder()
                        .blank()
                        .next(C.aqua + C.bold + "Server Name:")
                        .next(UtilServer.getServerName())
                        .blank()
                        .next(C.aqua + C.bold + "Rank:")
                        .next(globalPlayer.getRank().getName(true, true, true))
                        .blank()
                        .next(C.aqua + C.bold + "Coins:")
                        .next(String.valueOf(globalPlayer.getCoins()))
                        .blank()
                        .next(C.aqua + C.bold + "Points:")
                        .next(String.valueOf(globalPlayer.getPoints()))
                        .blank()
                        .next("" + C.red + C.bold + "Server Website:")
                        .next("Tropilus.com")
                        .build();
            }

        }).setUpdateInterval(5).activate();

         UtilEntity.setNameTag(event.getPlayer(), globalPlayer.getRank().getName(true, true, true));
    }

}
