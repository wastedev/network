package com.infinity.hub.cosmetic.companions.type;

import com.infinity.hub.cosmetic.companions.companion.Companion;
import com.infinity.hub.cosmetic.companions.companion.part.CompanionModuleAnimated;
import com.infinity.hub.cosmetic.companions.companion.part.CompanionPart;
import com.infinity.hub.cosmetic.companions.companion.part.armorstand.CompanionEquipment;
import com.infinity.hub.cosmetic.companions.companion.part.armorstand.CompanionOptions;
import me.adamtanana.core.util.builder.ArmorBuilder;
import me.adamtanana.core.util.builder.SkullBuilder;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class Shrek extends Companion {

    public Shrek(Player player) {
        super(player, EntityType.PIG, CompanionType.CHIMP);

        ItemStack head = new SkullBuilder().setCustomSkull("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNDg2OGRkNTBiY2I3M2M0N2M0YWFlZTc1YzdlYjNjNzA5Nzg4NWQ0YTlkZDM0YTU3YzhjYTQ4ZGUzYjc2NTk4YSJ9fX0=").build();
        ItemStack bodyTop = new ArmorBuilder(new ItemStack(Material.LEATHER_CHESTPLATE)).setColor(Color.fromRGB(193, 209, 51)).build();
        ItemStack bodyBottom = new ArmorBuilder(new ItemStack(Material.LEATHER_LEGGINGS)).setColor(Color.fromRGB(84, 59, 30)).build();
        ItemStack boots = new ArmorBuilder(new ItemStack(Material.LEATHER_BOOTS)).setColor(Color.fromRGB(193, 209, 51)).build();
        CompanionPart[] parts = new CompanionPart[] {
            new CompanionModuleAnimated(this, "head", 1, 0, 0F, 0F, 0F, new CompanionEquipment().setHelmet(head).setChestplate(bodyTop).setLeggings(bodyBottom), new CompanionOptions(false, false, false, true)),
        };

        addCompanionParts(parts);
    }
}
