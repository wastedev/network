package com.infinity.hub.cosmetic;

import com.infinity.hub.cosmetic.companions.PetMenu;
import com.infinity.hub.cosmetic.companions.companion.CompanionManager;
import com.infinity.hub.cosmetic.gadget.GadgetMenu;
import me.adamtanana.core.inventory.InfinityMenu;
import me.adamtanana.core.inventory.item.ClickableItem;
import me.adamtanana.core.util.builder.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.Overridden;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;

public class CosmeticMenu extends InfinityMenu {

    public CosmeticMenu(CosmeticManager cosmeticManager){
        super(6, "Cosmetics");

        addItem(new ClickableItem(10, new ItemBuilder(Material.ENDER_CHEST).setName("Gadgets").build(), true) {
            @Override
            public void click(Player player, ClickType clickType) {
                new GadgetMenu(cosmeticManager.getGadgetManager()).openInventory(player);
            }
        });

        addItem(new ClickableItem(11, new ItemBuilder(Material.MONSTER_EGG).setName("Pets").build(), true) {
            @Overridden
            public void click(Player player, ClickType type) {
                new PetMenu(cosmeticManager.getCompanionModule().getCompanionManager()).openInventory(player);
            }
        });
    }
}
