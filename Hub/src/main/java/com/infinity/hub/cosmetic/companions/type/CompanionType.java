package com.infinity.hub.cosmetic.companions.type;

import com.infinity.hub.cosmetic.companions.companion.Companion;
import me.adamtanana.core.util.C;
import me.adamtanana.core.util.builder.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public enum CompanionType
{
	MC_8(
			C.gold + "Star" + C.white + "Bot " + C.reset ,
			new String[]{"Description line1", "line2", "ect.."},
			1,
			BB8.class,
            Material.NETHER_STAR,
            10
	),

	DUCK(
			C.yellow + "Duck " + C.reset ,
			new String[]{"Description line1", "line2", "ect.."},
			2,
			Duck.class,
            Material.YELLOW_FLOWER,
            5
	),

	GORILLA(
			C.white + "Gorilla " + C.reset ,
			new String[]{"Description line1", "line2", "ect.."},
			3,
			Gorilla.class,
            Material.NETHER_BRICK_ITEM,
            10
	),

	CHIMP(
			C.white + "Chimp " + C.reset ,
			new String[]{"Description line1", "line2", "ect.."},
			4,
			Chimp.class,
            Material.BRICK,
            10
	),

	PUG(
			C.aqua + "Pug " + C.reset ,
			new String[]{"Description line1", "line2", "ect.."},
			5,
			Gorilla.class,
            Material.BONE,
            10
	),

	MINION(
			C.yellow + "Minion " + C.reset ,
			new String[]{"Description line1", "line2", "ect.."},
			6,
			Gorilla.class,
            Material.WOOD_HOE,
            15
	),

	TURTLE(
			C.green + "Turtle " + C.reset,
			new String[]{"Description line1", "line2", "ect.."},
			7,
			Gorilla.class,
            Material.WATER_LILY,
            15
	);

	private String name;
	private String[] description;
	private int id, price;
	private Class<? extends Companion> companionClass;
	private Material displayItem;


	CompanionType(String name, String[] description, int id, Class<? extends Companion> companionClass, Material displayItem, int price)
	{
		this.name = name;
		this.description = description;
		this.id = id;
		this.companionClass = companionClass;
	    this.displayItem = displayItem;
        this.price = price;
    }

	public String getName()
	{
		return name;
	}

	public String[] getDescription()
	{
		return description;
	}

    public int getId()
    {
        return id;
    }

    public int getPrice()
    {
        return price;
    }

    public Material getDisplayItem()
    {
        return displayItem;
    }

    public Class<? extends Companion> getCompanionClass()
	{
		return companionClass;
	}
}
