package com.infinity.hub.cosmetic.companions.command;

import com.infinity.hub.cosmetic.companions.companion.CompanionManager;
import com.infinity.hub.cosmetic.companions.type.CompanionType;
import me.adamtanana.core.player.Rank;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Slime;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class StatueCommand extends me.adamtanana.core.command.Command
{
	private CompanionManager companionManager;

	public StatueCommand(CompanionManager companionManager) {
		super("statue", new ArrayList<>(), 1, Rank.OWNER);

		this.companionManager = companionManager;
	}

	@Override
	public void execute(Player player, List<String> args) {
		Location lookAt = player.getLocation();

		if(args.get(0).equalsIgnoreCase("BB8")) companionManager.spawnCompanionStatue(player.getLocation(), lookAt, CompanionType.MC_8);
		else if(args.get(0).equalsIgnoreCase("Duck")) companionManager.spawnCompanionStatue(player.getLocation(), lookAt, CompanionType.DUCK);
		else if(args.get(0).equalsIgnoreCase("Gorilla")) companionManager.spawnCompanionStatue(player.getLocation(), lookAt, CompanionType.GORILLA);
		else if(args.get(0).equalsIgnoreCase("Dog")) companionManager.spawnCompanionStatue(player.getLocation(), lookAt, CompanionType.PUG);
		else if(args.get(0).equalsIgnoreCase("Chimp")) companionManager.spawnCompanionStatue(player.getLocation(), lookAt, CompanionType.CHIMP);
		else if(args.get(0).equalsIgnoreCase("Turtle")) companionManager.spawnCompanionStatue(player.getLocation(), lookAt, CompanionType.TURTLE);
		else if(args.get(0).equalsIgnoreCase("Minion")) companionManager.spawnCompanionStatue(player.getLocation(), lookAt, CompanionType.MINION);


	}
}
