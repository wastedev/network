package com.infinity.hub.cosmetic.gadget;

import com.google.common.collect.Lists;
import me.adamtanana.core.inventory.InfinityMenu;
import me.adamtanana.core.inventory.item.ClickableItem;
import me.adamtanana.core.player.GadgetEntry;
import me.adamtanana.core.player.GlobalPlayer;
import me.adamtanana.core.util.CommonUtil;
import me.adamtanana.core.util.builder.ItemBuilder;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;

import java.util.Queue;

public class GadgetMenu extends InfinityMenu {

    public GadgetMenu(GadgetManager gadgetManager) {
        super(6, "Gadgets");

        int[] slots = new int[]{10,11,12,13,14,15,16,19,20,21,22,23,24,25,28,29,30,31,32,33,34,37,38,39,40,41,42,43};
        Queue<GadgetType> gadgets = Lists.newLinkedList();
        for(GadgetType gadgetType : GadgetType.values()) {
            gadgets.add(gadgetType);
        }

        for(int i = 0; i < slots.length; i++) {
            try {
                if(gadgets.isEmpty()) break;
                GadgetType gadgetType = gadgets.poll();
                Gadget gadget = gadgetType.getGadget().newInstance();
                addItem(new ClickableItem(slots[i], new ItemBuilder(gadget.getDisplayMaterial()).setName(gadget.getDisplayName()).build(), true) {
                    @Override
                    public void click(Player player, ClickType clickType) {
                        boolean hasGadget = false;
                        GlobalPlayer globalPlayer = CommonUtil.getGlobalPlayer(player.getUniqueId());
                        for(GadgetEntry gadgetEntry : globalPlayer.getAllGadgets()) {
                            if(!gadgetEntry.getGadgetName().equals(gadgetType.name())) continue;
                            hasGadget = true;
                            break;
                        }

                        if(!hasGadget) {
                            player.sendMessage("You do not have this gadget unlocked.");
                            return;
                        }

                        gadgetManager.removeGadget(player.getUniqueId());
                        gadgetManager.setGadget(player.getUniqueId(), gadgetType);
                        player.closeInventory();
                        player.sendMessage("Gadget selected, " + gadget.getDisplayName());
                    }
                });
            } catch (InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
                continue;
            }
        }
    }
}
