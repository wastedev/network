package com.infinity.hub.cosmetic.companions;


import com.google.common.collect.Lists;
import com.infinity.hub.cosmetic.companions.companion.CompanionManager;
import com.infinity.hub.cosmetic.companions.type.CompanionType;
import me.adamtanana.core.inventory.InfinityMenu;
import me.adamtanana.core.inventory.item.ClickableItem;
import me.adamtanana.core.player.GlobalPlayer;
import me.adamtanana.core.util.C;
import me.adamtanana.core.util.CommonUtil;
import me.adamtanana.core.util.builder.ItemBuilder;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;

import java.util.Arrays;
import java.util.Queue;

public class PetMenu extends InfinityMenu {

    public PetMenu(CompanionManager manager) {
        super(6, "Pets");

        int[] slots = new int[]{10, 11, 12, 13, 14, 15, 16, 19, 20, 21, 22, 23, 24, 25, 28, 29, 30, 31, 32, 33, 34, 37, 38, 39, 40, 41, 42, 43};
        Queue<CompanionType> gadgets = Lists.newLinkedList();
        for (CompanionType companionType : CompanionType.values()) {
            gadgets.add(companionType);
        }

        for (int i = 0; i < slots.length; i++) {
            if (gadgets.isEmpty()) break;
            CompanionType companionType = gadgets.poll();
            addItem(new ClickableItem(slots[i], new ItemBuilder(companionType.getDisplayItem()).setName(companionType.getName()).setLore(Arrays.asList(companionType.getDescription())).build(), true) {
                @Override
                public void click(Player player, ClickType clickType) {
                    if(clickType == ClickType.LEFT) {
                        GlobalPlayer gp = CommonUtil.getGlobalPlayer(player.getUniqueId());
                        if(gp.getAllPets().contains(companionType.getId())) {
                            manager.spawnCompanion(player, companionType);
                            player.sendMessage(C.lightPurple + "Pet spawned");
                        }
                    } else if(clickType == ClickType.RIGHT) { //Unspawn
                        manager.removePlayerCompanion(player);
                    }
                }
            });
        }
    }
}
