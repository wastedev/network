package com.infinity.hub.cosmetic;

import com.google.common.collect.Lists;
import com.infinity.hub.cosmetic.companions.CompanionModule;
import com.infinity.hub.cosmetic.gadget.GadgetManager;
import me.adamtanana.core.Module;
import me.adamtanana.core.util.UtilServer;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class CosmeticManager extends Module {

    private List<Module> managers;
    private GadgetManager gadgetManager;
    private CompanionModule companionModule;

    public CosmeticManager() {
        managers = Lists.newArrayList();

        gadgetManager = new GadgetManager();
        companionModule = new CompanionModule();
        managers.add(gadgetManager);
        managers.add(companionModule);
    }

    public GadgetManager getGadgetManager() {
        return gadgetManager;
    }

    public CompanionModule getCompanionModule() {
        return companionModule;
    }

    public void enable() {
        for (Module m : managers) {
            UtilServer.registerListener(m);
            m.enable();
        }
    }

    public void disable() {
        for(Module m : managers) {
            m.disable();
            UtilServer.unregisterListener(m);
        }
        managers.clear();
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        ItemStack item = event.getItem();
        if(item == null || item.getType() == Material.AIR) return;

        switch (item.getType()) {
            case CHEST:
                new CosmeticMenu(this).openInventory(event.getPlayer());
                break;
        }
    }
}
