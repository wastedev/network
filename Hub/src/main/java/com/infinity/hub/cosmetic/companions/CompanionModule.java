package com.infinity.hub.cosmetic.companions;

import com.infinity.hub.cosmetic.companions.command.GivePetCommand;
import com.infinity.hub.cosmetic.companions.command.StatueCommand;
import com.infinity.hub.cosmetic.companions.companion.CompanionManager;
import me.adamtanana.core.Module;
import me.adamtanana.core.command.CommandHandler;
import me.adamtanana.core.packet.PacketEvent;
import me.adamtanana.core.reflection.SafeClass;
import me.adamtanana.core.reflection.SafeField;
import me.adamtanana.core.util.UtilServer;
import net.minecraft.server.v1_8_R3.PacketPlayOutSpawnEntity;
import net.minecraft.server.v1_8_R3.PacketPlayOutSpawnEntityLiving;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntitySpawnEvent;

public class CompanionModule extends Module {

    private static CompanionManager companionManager;

    @Override
    public void enable() {
        companionManager = new CompanionManager(UtilServer.getPlugin());

        CommandHandler.register(new StatueCommand(companionManager));
        CommandHandler.register(new GivePetCommand());

        for(World world : Bukkit.getWorlds())
        {
            for(Entity entity : world.getEntities())
            {
                if(entity.getCustomName() == null) continue;
                if(!entity.getCustomName().equals("companion")) continue;
                for(Entity entity2 : entity.getNearbyEntities(2, 2, 2))
                {
                    if(!(entity2 instanceof ArmorStand)) continue;
                    if(((ArmorStand) entity2).isVisible()) continue;
                    if(entity2.getCustomName() != null) continue;
                    entity2.remove();
                }
                entity.remove();
            }
        }
    }

    @Override
    public void disable() {
        companionManager.removeAllCompanions();
	    companionManager.removeAllStatues();
    }

    public static CompanionManager getCompanionManager() {
        return companionManager;
    }
}
