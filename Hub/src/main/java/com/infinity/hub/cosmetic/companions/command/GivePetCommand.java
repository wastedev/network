package com.infinity.hub.cosmetic.companions.command;

import com.infinity.hub.cosmetic.companions.companion.Companion;
import com.infinity.hub.cosmetic.companions.type.CompanionType;
import com.infinity.hub.cosmetic.gadget.GadgetType;
import me.adamtanana.core.command.Command;
import me.adamtanana.core.player.GlobalPlayer;
import me.adamtanana.core.player.Rank;
import me.adamtanana.core.util.C;
import me.adamtanana.core.util.CommonUtil;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.List;

public class GivePetCommand extends Command {

    public GivePetCommand() {
        super("givepet", Arrays.asList(), 2, Rank.ADMIN);
    }

    //givepet 582 1

    @Override
    public void execute(Player player, List<String> args) {
        String receiver = args.get(0);

        if(Bukkit.getPlayer(receiver) == null) {
            player.sendMessage(C.red + "Player not online!");
            return;
        }
        CompanionType type = null;
        try {
            type = CompanionType.values()[Integer.valueOf(args.get(1)) + 1];
        } catch (Exception e) {
            player.sendMessage(C.yellow + args.get(1) + C.red + " is not a valid gadget!");
            return;
        }

        player.sendMessage(C.yellow + receiver + C.darkAqua + " has received " + type.getName());
        Bukkit.getPlayer(receiver).sendMessage(C.yellow +"You received " + type.getName());

        GlobalPlayer gp = CommonUtil.getGlobalPlayer(Bukkit.getPlayer(receiver).getUniqueId());
        gp.addPet(type.getId());
    }
}
