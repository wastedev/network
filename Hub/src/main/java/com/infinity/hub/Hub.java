package com.infinity.hub;

import com.infinity.hub.cosmetic.CosmeticManager;
import com.infinity.hub.cosmetic.companions.type.CompanionType;
import com.infinity.hub.listener.ChatHandler;
import com.infinity.hub.listener.DoubleJump;
import com.infinity.hub.listener.JoinHandler;
import me.adamtanana.core.Core;
import me.adamtanana.core.event.UpdateEvent;
import me.adamtanana.core.listeners.*;
import me.adamtanana.core.npc.*;
import me.adamtanana.core.redis.ServerType;
import me.adamtanana.core.redis.message.SendGametypeMessage;
import me.adamtanana.core.util.C;
import me.adamtanana.core.util.UtilLocation;
import me.adamtanana.core.util.UtilServer;
import me.adamtanana.core.util.UtilWorld;
import net.minecraft.server.v1_8_R3.PacketPlayInUseEntity;
import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.*;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

public class Hub extends Core {


    private static Location spawn;

    public static Location getSpawn() {
        return spawn;
    }

    private static UtilWorld.WorldData worldData;
    private boolean spawnedMobs = false;

    @Override
    protected void enable() {
        enableModules(new PlayerScoreboard(), new CosmeticManager());

        enableModules(new Blocks(), new Damage(), new DoubleJump(), new FarmTrample(), new Hunger(), new Item(), new JoinHandler(), new LeavesDecay(), new Quit(), new ChatHandler());
        UtilWorld.loadWorld("Lobby", "Hub", (worldData) -> {
            spawn = worldData.getDataPoints().get(DyeColor.BLACK).get(0).add(0.5, 0.5, 0.5);

            Hub.this.worldData = worldData;

            Location gameLookAt = worldData.getDataPoints().get(DyeColor.YELLOW).get(0).add(0.5, 0.5, 0.5);

            spawn = UtilLocation.lookAt(spawn, gameLookAt, true, true);
            Location escape = UtilLocation.lookAt(worldData.getDataPoints().get(DyeColor.RED).get(0).add(0.5, 0, 0.5), gameLookAt, true, true);


            getModule(NPCManager.class).spawnPlayer(
                    "Escape",
                    escape,
                    "eyJ0aW1lc3RhbXAiOjE0NjgzNjkwNDc2NTgsInByb2ZpbGVJZCI6IjcxZmZhMzg5ZGEwYTQ1YTZiMDM4NDM3YjQ1YTcyNWU3IiwicHJvZmlsZU5hbWUiOiJNdW1teSIsInNpZ25hdHVyZVJlcXVpcmVkIjp0cnVlLCJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvM2U5MWU5NTgyMmZlOThjYzVhNTY1OGU4MjRiMWI4Y2YxNGQ0ZGU5MmYwZTFhZjI0ODE1MzcyNDM1YzllYWI2In19fQ==",
                    "IQPeyrtDVmnBO8N8TJnrymzDduB+sfa5DHXWBcPB+prgHDdYQse2YQt9Vxn3rRrP3MVJr4dJbKSmrXyn2IZSqBc5dBZ24lP2TprnZcBWX/KT9I2xv0slQVspxODCfyYk0bjITO1LLbk8KjluXOiRX4dbxvTD6eKogipabmUZmbCSYvXfTABvA4iNQdBYrTEN7tFzS5d3d1Cz8IWGYUHWpGSRnvPu62aLl0NDZ5pf/4wQOFiQtJGjujDzV82zp+57UIYLaP5gWa26j1xLYCiDaTAmU36D8/K3wf9cEWyezNIgkfWZ0mHVGBLafOOH8G4TP1Yd+OaUNupkrg3/XjqqDVpxqUnwmUKYmgzFNup4Jy9Fs+H3SHOqds3ToYSmOPVScAxTOcah+ymn+2rGh7cdGuz7IXbIb8s/oMJhpCbE+CVGCI7e6NvABdNOTW3elEMvcWkAoR2Bano3SO7FW/7gDIFjI2OIC8CA/7knap7/nSZjei2CaSTqkWWtQUcOFdOA7UMLrqPLHVacZYDnZLFgkKKgVVXC6uLjH53njdkoO6PJcj57VpVjLuCFjKOOVWfV21yDXcgc1O9OTRxUaR/jkSbWkkSYmmLfJvR5ruLgGeVne6t4UkaK4FvyML7vEr/MJc9IhatXIEKRcBSKNqKBkjJJ8agbleM56WclOkiF+Qc="
            );
        });

    }

    @Override
    public void disable() {
        UtilWorld.unloadWorldSync("Hub");
    }

    @Override
    public ServerType getServerType() {
        return ServerType.HUB;
    }

    @EventHandler
    public void spawnCompanions(PlayerJoinEvent e) {
        if(spawnedMobs) return;
        spawnedMobs = true;
        Location petLookAt = worldData.getDataPoints().get(DyeColor.LIME).get(0).add(0.5, 0.5, 0.5);
        UtilServer.runTaskLater(() -> {

            for (int i = 0; i < worldData.getDataPoints().get(DyeColor.WHITE).size(); i++) {
                Location loc = worldData.getDataPoints().get(DyeColor.WHITE).get(i).add(0.5, 0, 0.5);
                UtilWorld.quickChangeBlock(worldData.getWorld(), loc, 0, 0);
                getModule(CosmeticManager.class).getCompanionModule().getCompanionManager().spawnCompanionStatue(loc, petLookAt, CompanionType.values()[i]);
            }
        }, 20L);

    }

    @EventHandler
    public void onSpawn(CreatureSpawnEvent e) {
        if(e.getSpawnReason() != CreatureSpawnEvent.SpawnReason.CUSTOM) e.setCancelled(true);
    }

    public static UtilWorld.WorldData getWorldData() {
        return worldData;
    }


    @EventHandler
    public void onPhysics(BlockPhysicsEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onWeather(WeatherChangeEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onGrass(BlockSpreadEvent e) {
        if(e.getBlock().getRelative(BlockFace.UP).getType() == Material.WOOL) e.setCancelled(true);
    }
    @EventHandler
    public void disableBurn(BlockBurnEvent event)
    {
        event.setCancelled(true);
    }

    @EventHandler
    public void disableIgnite(BlockIgniteEvent event)
    {
        if (event.getCause() == BlockIgniteEvent.IgniteCause.LAVA || event.getCause() == BlockIgniteEvent.IgniteCause.SPREAD)
            event.setCancelled(true);
    }

    @EventHandler
    public void disableFire(BlockSpreadEvent event)
    {
        event.setCancelled(true);
    }

    @EventHandler
    public void disableFade(BlockFadeEvent event)
    {
        event.setCancelled(true);
    }

    @EventHandler
    public void disableDecay(LeavesDecayEvent event)
    {
        event.setCancelled(true);
    }

    @EventHandler
    public void disableIceForm(BlockFormEvent event)
    {
        event.setCancelled(true);
    }

    @EventHandler
    public void onNpc(NPCInteractEvent e) {
        if(e.getAction() == PacketPlayInUseEntity.EnumEntityUseAction.ATTACK) {
            NPC npc = e.getNPC();
            String name = npc.getName().toUpperCase();
            e.getPlayer().sendMessage(C.bold + "Locating server...");
            getJedis().sendMessage(new SendGametypeMessage(e.getPlayer().getName(), ServerType.valueOf(name)), "bungee");
        }
    }

    @EventHandler
    public void aVoid(UpdateEvent e) {
        Bukkit.getOnlinePlayers().stream().filter(p -> p.getLocation().getBlockY() <= worldData.getMinY()).forEach(p -> {
            p.teleport(getSpawn());
        });
    }
}
