package com.infinity.hub.listener;

import com.infinity.hub.Hub;
import com.infinity.hub.cosmetic.CosmeticManager;
import com.infinity.hub.cosmetic.CosmeticMenu;
import com.infinity.hub.cosmetic.gadget.GadgetManager;
import me.adamtanana.core.Module;
import me.adamtanana.core.player.GlobalPlayer;
import me.adamtanana.core.player.Rank;
import me.adamtanana.core.util.C;
import me.adamtanana.core.util.CommonUtil;
import me.adamtanana.core.util.UtilServer;
import me.adamtanana.core.util.builder.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;

public class JoinHandler extends Module {

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        Player p = e.getPlayer();
        UtilServer.runTaskLater(() -> p.teleport(Hub.getSpawn()), 5L);
        p.getInventory().clear();


        //Set inv
        p.getInventory().setItem(4, new ItemBuilder(Material.CHEST).setName(C.green + "Gadgets").build());

        p.setGameMode(GameMode.SURVIVAL);
        e.setJoinMessage(null);

        p.spigot().setCollidesWithEntities(false);
        p.setHealth(20);
        p.setFoodLevel(20);
        p.setFireTicks(0);
        p.setFlying(false);
        p.setAllowFlight(true);
    }

    @EventHandler
    public void onRespawn(PlayerRespawnEvent e) {
        e.setRespawnLocation(Hub.getSpawn());
    }
}