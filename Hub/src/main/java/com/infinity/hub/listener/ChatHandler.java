package com.infinity.hub.listener;

import me.adamtanana.core.Module;
import me.adamtanana.core.player.GlobalPlayer;
import me.adamtanana.core.util.C;
import me.adamtanana.core.util.CommonUtil;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatHandler extends Module{

    @EventHandler
    public void onPlayerChat(AsyncPlayerChatEvent event) {
        GlobalPlayer globalPlayer = CommonUtil.getGlobalPlayer(event.getPlayer().getUniqueId());
        if(globalPlayer.isLoaded()) {
            event.setFormat(globalPlayer.getRank().getName(true, true, true) + globalPlayer.getRank().getColor() + " " + event.getPlayer().getName() + " " + C.white + event.getMessage());
        } else {
            event.setCancelled(true);
            event.getPlayer().sendMessage(C.red + "Your profile hasn't loaded yet.");
        }
    }
}
