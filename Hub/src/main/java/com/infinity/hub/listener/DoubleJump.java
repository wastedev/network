package com.infinity.hub.listener;

import me.adamtanana.core.Module;
import me.adamtanana.core.util.UtilTime;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.util.Vector;

public class DoubleJump extends Module {

    @EventHandler
    public void move(PlayerMoveEvent event) {
        if(event.getPlayer().getLocation().getBlock() == null) return;
        if(event.getPlayer().getLocation().getBlock().getType() == Material.GOLD_PLATE) {
            if (UtilTime.useAbility(event.getPlayer(), "pressurejump", 500, false)) {
                event.getPlayer().setVelocity(event.getPlayer().getLocation().getDirection().multiply(7).setY(1.5));
            }
        }
    }

    @EventHandler
    public void onToggleFlight(PlayerToggleFlightEvent event) {
        Player player = event.getPlayer();

        if (player.getGameMode() == GameMode.CREATIVE)
            return;

        event.setCancelled(true);
        player.setAllowFlight(false);
        Vector velocity = player.getLocation().getDirection().multiply(4);
        velocity.setY(velocity.getY() + 0.5);
        player.setVelocity(velocity);
        player.playSound(player.getLocation(), Sound.EXPLODE, 1f, 1f);
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event) {
        Player player = event.getPlayer();

        if (player.getGameMode() != GameMode.CREATIVE && ((Entity) player).isOnGround()) {
            if (!player.isFlying()) {
                player.setAllowFlight(true);
            }
        }
    }

}
