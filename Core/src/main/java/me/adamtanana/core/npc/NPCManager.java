package me.adamtanana.core.npc;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import me.adamtanana.core.Module;
import me.adamtanana.core.event.UpdateEvent;
import me.adamtanana.core.event.UpdateType;
import me.adamtanana.core.packet.PacketEvent;
import me.adamtanana.core.redis.ServerType;
import me.adamtanana.core.redis.message.SendGametypeMessage;
import me.adamtanana.core.reflection.SafeClass;
import me.adamtanana.core.reflection.SafeField;
import me.adamtanana.core.util.C;
import me.adamtanana.core.util.UtilServer;
import net.minecraft.server.v1_8_R3.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerQuitEvent;

import java.lang.reflect.Field;
import java.util.*;

public class NPCManager extends Module {

    private HashMap<NPC, List<Player>> cache = Maps.newHashMap();

    public void spawnPlayer(String n, Location loc, String skinValue, String skinSignature) {
        String realName;
        if (n.length() > 16) realName = n.substring(0, 16);
        else realName = n;

        GameProfile profile = new GameProfile(UUID.randomUUID(), realName);
        profile.getProperties().put("textures", new Property("textures", skinValue, skinSignature));

        int uid = new Random().nextInt(Integer.MAX_VALUE);
        boolean found;
        do{
            found = false;
            for(Entity e : loc.getWorld().getEntities()) {
                if(e.getEntityId() == uid) {
                    found = true;
                    uid = new Random().nextInt(Integer.MAX_VALUE-10000) + 5000;
                    break;
                }
            }
        } while (found);

        NPC npc = new NPC(realName, loc, profile, profile.getId(), uid);
        cache.put(npc, Lists.newArrayList());

    }

    private void sendPackets(NPC npc, Player... players) throws Exception {
        sendPackets(npc, Arrays.asList(players));
    }

    private void sendPackets(NPC npc, Collection<Player> players) throws Exception {
        if(players.isEmpty()) return;
        /** Player List **/
        PacketPlayOutPlayerInfo listPacket = new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER);
        SafeField playerListField = new SafeField(PacketPlayOutPlayerInfo.class.getDeclaredField("b"));
        playerListField.getHandle().setAccessible(true);

        List<PacketPlayOutPlayerInfo.PlayerInfoData> b = (List<PacketPlayOutPlayerInfo.PlayerInfoData>) playerListField.get(listPacket);
        b.add(listPacket.new PlayerInfoData(npc.getProfile(), 1, WorldSettings.EnumGamemode.SURVIVAL, new ChatMessage(npc.getName())));

        /** Entity **/
        PacketPlayOutNamedEntitySpawn spawnPacket = new PacketPlayOutNamedEntitySpawn();
        setValue(PacketPlayOutNamedEntitySpawn.class.getDeclaredField("a"), spawnPacket, npc.getEid());
        setValue(PacketPlayOutNamedEntitySpawn.class.getDeclaredField("b"), spawnPacket, npc.getUuid());
        setValue(PacketPlayOutNamedEntitySpawn.class.getDeclaredField("c"), spawnPacket, MathHelper.floor(npc.getLoc().getX() * 32.0D));
        setValue(PacketPlayOutNamedEntitySpawn.class.getDeclaredField("d"), spawnPacket, MathHelper.floor(npc.getLoc().getY() * 32.0D));
        setValue(PacketPlayOutNamedEntitySpawn.class.getDeclaredField("e"), spawnPacket, MathHelper.floor(npc.getLoc().getZ() * 32.0D));
        setValue(PacketPlayOutNamedEntitySpawn.class.getDeclaredField("f"), spawnPacket, (byte) ((int) (npc.getLoc().getYaw() * 256.0F / 360.0F)));
        setValue(PacketPlayOutNamedEntitySpawn.class.getDeclaredField("g"), spawnPacket, (byte) ((int) (npc.getLoc().getPitch() * 256.0F / 360.0F)));
        setValue(PacketPlayOutNamedEntitySpawn.class.getDeclaredField("h"), spawnPacket, 0); //item in hand
        setValue(PacketPlayOutNamedEntitySpawn.class.getDeclaredField("i"), spawnPacket, new DataWatcher(((CraftPlayer) UtilServer.getRandomPlayer()).getHandle())); //item in hand
        setValue(PacketPlayOutNamedEntitySpawn.class.getDeclaredField("j"), spawnPacket, Lists.newArrayList()); //item in hand

        UtilServer.broadcastPacket(players, listPacket, spawnPacket);

        /** Delete name **/
        UtilServer.runTaskLater(() -> {
            try {
                setValue(PacketPlayOutPlayerInfo.class.getDeclaredField("a"), listPacket, PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER);
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
            UtilServer.broadcastPacket(players, listPacket);
            try {
                PacketPlayOutEntity.PacketPlayOutRelEntityMoveLook moveLook = new PacketPlayOutEntity.PacketPlayOutRelEntityMoveLook(
                        npc.getEid(),
                        (byte) 0, (byte) 0, (byte) 0,
                        (byte) ((int) (npc.getLoc().getYaw() * 256.0F / 360.0F)),
                        (byte) ((int) (npc.getLoc().getPitch() * 256.0F / 360.0F)),
                        false
                );
                PacketPlayOutEntityHeadRotation rot = new PacketPlayOutEntityHeadRotation();
                setValue(PacketPlayOutEntityHeadRotation.class.getDeclaredField("a"), rot, npc.getEid());
                setValue(PacketPlayOutEntityHeadRotation.class.getDeclaredField("b"), rot, (byte) ((int) (npc.getLoc().getYaw() * 256.0F / 360.0F)));

                UtilServer.broadcastPacket(players, moveLook, rot);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }, 5L);
    }

    private void setValue(Field f, Object instance, Object value) {
        new SafeField(f).set(instance, value);
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        cache.forEach((s, list) -> list.remove(e.getPlayer()));
    }
    @EventHandler
    public void update(UpdateEvent e) {
        if (e.getType() != UpdateType.SEC) return;
        if(Bukkit.getOnlinePlayers().size() == 0) return;

        int maxDistance = 128 * 128;
        int minDistance = 60 * 60;

        for (NPC npc : cache.keySet()) {
            List<Player> players = Lists.newArrayList();
            for (Player player : Bukkit.getOnlinePlayers()) {
                if (cache.get(npc).contains(player)) {
                    if(player.getLocation().distanceSquared(npc.getLoc()) > maxDistance) {
                        cache.get(npc).remove(player);
                    }
                    continue;
                }
                double distanceS = player.getLocation().distanceSquared(npc.getLoc());
                if (distanceS < minDistance) {
                    players.add(player);
                    cache.get(npc).remove(player);
                    cache.get(npc).add(player);
                }
            }
            if (players.isEmpty()) continue;
            try {
                sendPackets(npc, players);
            } catch (Exception e1) {
                e1.printStackTrace();
            }

        }
    }

    @EventHandler
    public void onInteract(PacketEvent e) {
        if(e.getPacketType() == PacketEvent.PacketType.OUTWARDS) return;
        if(e.getPacket() instanceof PacketPlayInUseEntity) {
            PacketPlayInUseEntity packet = (PacketPlayInUseEntity) e.getPacket();

            SafeClass packetClazz = new SafeClass(PacketPlayInUseEntity.class);
            int eid = packetClazz.getField("a").get(packet, int.class);

            cache.keySet().stream().filter(n -> n.getEid() == eid).findFirst().ifPresent(n -> {
                PacketPlayInUseEntity.EnumEntityUseAction action = packet.a();
                NPCInteractEvent event = new NPCInteractEvent(n, e.getPlayer(), action);
                UtilServer.callEvent(event);
            });

        }

    }
}
