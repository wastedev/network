package me.adamtanana.core.npc;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.minecraft.server.v1_8_R3.PacketPlayInUseEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

@AllArgsConstructor
public class NPCInteractEvent extends Event {
    private static final HandlerList handlers = new HandlerList();

    @Getter
    private NPC NPC;
    @Getter
    private Player player;
    @Getter
    private PacketPlayInUseEntity.EnumEntityUseAction action;

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
