package me.adamtanana.core.scoreboard.common.animate;

public interface AnimatableString {

    String current();

    String next();

    String previous();

}
