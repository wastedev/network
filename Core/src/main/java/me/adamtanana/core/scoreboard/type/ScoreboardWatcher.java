package me.adamtanana.core.scoreboard.type;

import com.google.common.collect.Maps;
import lombok.Getter;
import me.adamtanana.core.Module;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.HashMap;
import java.util.UUID;

public class ScoreboardWatcher extends Module {
    private static HashMap<UUID, Scoreboard> scoreboards = Maps.newHashMap();

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        scoreboards.remove(event.getPlayer().getUniqueId());
    }

    public static void removeScoreboard(Player p) {
        if(scoreboards.remove(p.getUniqueId()) != null) {
            scoreboards.get(p.getUniqueId()).deactivate();
        }
    }

    public static void register(Scoreboard scb) {
        scoreboards.put(scb.getHolder().getUniqueId(), scb);
    }

}
