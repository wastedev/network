package me.adamtanana.core.packet;

import me.adamtanana.core.Module;
import me.adamtanana.core.player.GlobalPlayer;
import me.adamtanana.core.event.PlayerLocaleChangeEvent;
import me.adamtanana.core.util.CommonUtil;
import me.adamtanana.core.util.UtilServer;
import net.minecraft.server.v1_8_R3.PacketPlayInSettings;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PacketModule extends Module {
    @Override
    public void enable() {
        Bukkit.getOnlinePlayers().forEach(PacketHandler::hook);
    }

    @Override
    public void disable() {
        Bukkit.getOnlinePlayers().forEach(PacketHandler::unHook);
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onJoin(PlayerJoinEvent event) {
        PacketHandler.hook(event.getPlayer());
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onQuit(PlayerQuitEvent event) {
        PacketHandler.unHook(event.getPlayer());
    }

    @EventHandler
    public void playerLocale(PacketEvent event) {
        if(event.getPacket() instanceof PacketPlayInSettings) {
            PacketPlayInSettings settings = (PacketPlayInSettings) event.getPacket();
            GlobalPlayer globalPlayer = CommonUtil.getGlobalPlayer(event.getPlayer().getUniqueId());
            if(!globalPlayer.getLocale().equals(settings.a())) {
                UtilServer.callEvent(new PlayerLocaleChangeEvent(event.getPlayer(), globalPlayer.getLocale(), settings.a()));
                globalPlayer.setLocale(settings.a());
            }
        }
    }
}
