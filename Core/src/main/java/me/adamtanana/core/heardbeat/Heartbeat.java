package me.adamtanana.core.heardbeat;

import me.adamtanana.core.Core;
import me.adamtanana.core.Module;
import me.adamtanana.core.event.UpdateEvent;
import me.adamtanana.core.event.UpdateType;
import me.adamtanana.core.redis.JedisServer;
import me.adamtanana.core.redis.Message;
import me.adamtanana.core.redis.MessageListener;
import me.adamtanana.core.redis.message.GetServerName;
import me.adamtanana.core.redis.message.HeartbeatCallMessage;
import me.adamtanana.core.redis.message.HeartbeatReplyMessage;
import me.adamtanana.core.util.UtilServer;
import me.adamtanana.core.util.UtilTime;
import org.apache.commons.lang.RandomStringUtils;
import org.bukkit.event.EventHandler;

public class Heartbeat extends Module implements MessageListener<HeartbeatReplyMessage> {
    private long lastReply = 0;
    private boolean waiting = false;

    public Heartbeat() {
        JedisServer.registerListener(HeartbeatReplyMessage.class, this);
    }

    @EventHandler
    public void update(UpdateEvent event) {
        if(event.getType() != UpdateType.FAST) return;

        if(!UtilServer.getServerName().contains("-")) return;

        if(lastReply > 0 && UtilTime.elapsed(lastReply, 5000)) { //5 seconds before last reply
            UtilServer.setServerName(RandomStringUtils.randomAlphanumeric(6));
            Core.getJedis().setName(UtilServer.getServerName());
            ((Core) UtilServer.getPlugin()).findName();
            lastReply = 0;
            waiting = false;
            UtilServer.log("Bungee hasn't replied in over 5 seconds!!!");
        } else if(!waiting && UtilTime.elapsed(lastReply, 500)) {
            waiting = true;
            Core.getJedis().sendMessage(new HeartbeatCallMessage(), "bungee");
        }
    }

    @Override
    public void onReceive(String sender, HeartbeatReplyMessage msg) {
//        if(!sender.equalsIgnoreCase("bungee")) return;
        waiting = false;
        lastReply = System.currentTimeMillis();
    }
}
