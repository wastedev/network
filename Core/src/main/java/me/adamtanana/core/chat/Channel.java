package me.adamtanana.core.chat;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Channel {

    ENGLISH("English", "en", new String[]{"en_AU", "en_CA", "en_GB", "en_NZ", "en_UD", "en_PT", "en_US"}),
    GERMAN("German", "ge", new String[]{"nds_DE", ""}),
    SPANISH("Spanish", "es", new String[]{"es_AR", "es_ES", "es_MX", "es_UY", "es_VE"}),
    FRENCH("French", "", null);

    private String channelName;
    private String localKey;
    private String[] locale;
}
