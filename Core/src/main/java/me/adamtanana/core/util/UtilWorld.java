package me.adamtanana.core.util;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.Getter;
import me.adamtanana.core.engine.GameType;
import net.minecraft.server.v1_8_R3.*;
import org.apache.commons.io.FileUtils;
import org.bukkit.*;
import org.bukkit.Chunk;
import org.bukkit.World;
import org.bukkit.WorldType;
import org.bukkit.craftbukkit.v1_8_R3.CraftChunk;
import org.bukkit.craftbukkit.v1_8_R3.CraftServer;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.util.CraftMagicNumbers;
import org.bukkit.entity.Entity;
import org.bukkit.event.world.WorldUnloadEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.FileUtil;

import java.io.File;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.util.*;
import java.util.regex.Pattern;

public class UtilWorld {
    @Getter
    private static File mapFolder;
    @Getter
    private static File worldFolder;
    @Getter
    private static File buildmapFolder;


    public static void init() {
        try {
            mapFolder = new File(Bukkit.getWorldContainer().getCanonicalPath() + "/../maps");
            buildmapFolder = new File(Bukkit.getWorldContainer().getCanonicalPath() + "/../buildmaps");
        } catch (Exception e) {
            e.printStackTrace();
        }
        worldFolder = Bukkit.getWorldContainer();
        mapFolder.mkdirs();
        buildmapFolder.mkdirs();
        worldFolder.mkdirs();
    }

    public static boolean hasMaps(GameType type) {
        File gameFolder = new File(mapFolder, type.name());
        List<File> worlds = Lists.newArrayList();

        if (gameFolder.exists() && gameFolder.isDirectory()) {
            for (File world : gameFolder.listFiles()) {
                if (world.isDirectory() && !world.isHidden()) {
                    worlds.add(world);
                }
            }
        }
        return worlds.size() > 0;
    }

    public static void loadRandomWorld(GameType gameType, Callback<WorldData> callback) {
        File gameFolder = new File(mapFolder, gameType.name());
        List<File> worlds = Lists.newArrayList();

        if (gameFolder.exists() && gameFolder.isDirectory()) {
            for (File world : gameFolder.listFiles()) {
                if (world.isDirectory() && !world.isHidden()) {
                    worlds.add(world);
                    System.out.println(world.getName());
                }
            }
        }

        if (worlds.size() > 0) {
            loadWorld(gameType.name(), worlds.get((new Random().nextInt(worlds.size()))).getName(), callback);
        } else {
            Bukkit.broadcastMessage("No maps found :(");
            callback.call(null);
        }

    }

    public static void loadWorld(String gameType, String mapName, Callback<WorldData> callback) {
        if (Bukkit.getWorld(mapName) != null) {
            return;
        }
        UtilServer.runTaskAsync(() -> {
            File map = new File(mapFolder, gameType + "/" + mapName);
            boolean exists = map.exists();
            if (!exists) UtilServer.log("MAP LOADING DOESNT EXIST!!! - " + gameType + "/" + mapName);
            try {
                if (exists) FileUtils.copyDirectory(map, new File(worldFolder, mapName));
                else FileUtils.copyDirectory(new File(mapFolder, "template"), new File(worldFolder, mapName));
            } catch (Exception e) {
                System.out.println("Error copying world over");
            }

            UtilServer.runTask(() -> {
                WorldCreator creator = new WorldCreator(mapName);
                creator.generateStructures(false);
                creator.type(WorldType.FLAT);
                World world = creator.createWorld();
                world.setAutoSave(false);
                world.setKeepSpawnInMemory(false);
                world.setThundering(false);
                world.setStorm(false);
                world.setDifficulty(Difficulty.EASY);
                world.setTime(0);

                world.setGameRuleValue("doDaylightCycle", "false");
                world.getEntities().forEach(Entity::remove);
                world.setSpawnLocation(0, 56, 0);
                if (callback != null) callback.call(new WorldData(world));
            });
        });
    }


    public static void loadBuildWorld(String gameType, String mapName, Callback<WorldData> callback) {
        if (Bukkit.getWorld(mapName) != null) {
            return;
        }
        UtilServer.runTaskAsync(() -> {
            File map = new File(buildmapFolder, gameType + "/" + mapName);
            boolean exists = map.exists();
            try {
                if (exists) FileUtils.copyDirectory(map, new File(worldFolder, mapName));
                else FileUtils.copyDirectory(new File(mapFolder, "template"), new File(worldFolder, mapName));
            } catch (Exception e) {
                System.out.println("Error copying world over");
            }

            UtilServer.runTask(() -> {
                WorldCreator creator = new WorldCreator(mapName);
                creator.generateStructures(false);
                creator.type(WorldType.FLAT);
                World world = creator.createWorld();
                world.setKeepSpawnInMemory(false);
                world.setThundering(false);
                world.setStorm(false);
                world.setDifficulty(Difficulty.EASY);

                world.setSpawnLocation(0, 56, 0);
                if (callback != null) callback.call(new WorldData(world));
            });
        });
    }

    public static void unloadWorld(String mapName, boolean teleport, Callback<Boolean> callback) {
        World world = Bukkit.getWorld(mapName);
        if (world == null) return;
        File folder = world.getWorldFolder();
        if (teleport) world.getPlayers().forEach(p -> p.teleport(Bukkit.getWorld("world").getSpawnLocation()));

//        try {
//            Bukkit.unloadWorld(world, false);
//        } catch (Exception e) {
//            System.out.println("Error unloading world");
//        }
        unloadWorld(world, false);
        clearRegionFiles(mapName);

        UtilServer.runTaskAsync(() -> {
            try {
                FileUtils.deleteQuietly(folder);
                if (callback != null) callback.call(true);
            } catch (Exception e) {
                System.out.println("Error deleting world folder");
                if (callback != null) callback.call(false);
            }
        });
    }

    @Deprecated
    public static void unloadWorldSync(String mapName) {
        World world = Bukkit.getWorld(mapName);
        if (world == null) return;
        File folder = world.getWorldFolder();
        world.getPlayers().forEach(p -> p.kickPlayer("Restarting..."));

//        try {
//            Bukkit.unloadWorld(world, false);
//        } catch (Exception e) {
//            System.out.println("Error unloading world");
//        }
        unloadWorld(world, false);
        clearRegionFiles(mapName);


        try {
            FileUtils.deleteQuietly(folder);
        } catch (Exception e) {
            System.out.println("Error deleting world folder");
        }

    }


    public static void unloadBuildWorld(String mapName, String gameType, Callback callback) {
        unloadWorld(mapName, true, callback);
    }

    public static void saveBuildWorld(String mapName, String gameType) {
        World world = Bukkit.getWorld(mapName);
        if (world == null) return;
        File folder = world.getWorldFolder();

        try {
            ((CraftWorld) world).getHandle().save(true, (IProgressUpdate) null);
        } catch (Exception e) {
            e.printStackTrace();
        }

        File map = new File(buildmapFolder, gameType + "/" + mapName);
        clearRegionFiles(mapName);

        try {
            FileUtils.copyDirectory(new File(folder, "region"), new File(map, "region"));
            FileUtil.copy(new File(folder, "level.dat"), new File(map, "level.dat"));
        } catch (Exception e) {
            System.out.println("Error copying region and level");
        }
    }

    public static void saveWorld(String mapName, String gameType, Callback<Boolean> callback) {
        World world = Bukkit.getWorld(mapName);
        if (world == null) return;
        File folder = world.getWorldFolder();
        try {
            ((CraftWorld) world).getHandle().save(true, (IProgressUpdate) null);
        } catch (Exception e) {
            e.printStackTrace();
        }


        UtilServer.runTaskLater(() -> {
            File map = new File(mapFolder, gameType + "/" + mapName);
            clearRegionFiles(mapName);

            try {
                FileUtils.copyDirectory(new File(folder, "region"), new File(map, "region"));
                FileUtil.copy(new File(folder, "level.dat"), new File(map, "level.dat"));
                callback.call(true);
            } catch (Exception e) {
                System.out.println("Error copying region and level");
                callback.call(false);
            }
        }, 40);
    }

    public static HashMap<String, List<String>> getAllBuildMaps() {
        HashMap<String, List<String>> result = Maps.newHashMap();
        for (File game : buildmapFolder.listFiles()) {
            if (game.isHidden()) continue;
            if (game.isDirectory()) {
                result.put(game.getName(), Lists.newArrayList());
                for (File map : game.listFiles()) {
                    result.get(game.getName()).add(map.getName());
                }
            }
        }
        return result;
    }

    public static void unloadWorld(World world, boolean save) {
        if (save) {
            try {
                ((CraftWorld) world).getHandle().save(true, (IProgressUpdate) null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        world.setAutoSave(save);

        CraftServer server = (CraftServer) UtilServer.getPlugin().getServer();
        CraftWorld craftWorld = (CraftWorld) world;

        Bukkit.getPluginManager().callEvent(new WorldUnloadEvent(((CraftWorld) world).getHandle().getWorld()));
        Iterator<net.minecraft.server.v1_8_R3.Chunk> chunkIterator = ((CraftWorld) world).getHandle().chunkProviderServer.chunks
                .values().iterator();

        for (Entity entity : world.getEntities()) {
            entity.remove();
        }

        while (chunkIterator.hasNext()) {
            net.minecraft.server.v1_8_R3.Chunk chunk = chunkIterator.next();
            chunk.removeEntities();
        }

        ((CraftWorld) world).getHandle().chunkProviderServer.chunks.clear();
        ((CraftWorld) world).getHandle().chunkProviderServer.unloadQueue.clear();
        try {
            Field f = server.getClass().getDeclaredField("worlds");
            f.setAccessible(true);
            Map<String, World> worlds = (Map<String, World>) f.get(server);
            worlds.remove(world.getName().toLowerCase());
            f.setAccessible(false);
        } catch (IllegalAccessException ex) {
            System.out.println("Error removing world from bukkit master list: " + ex.getMessage());
        } catch (NoSuchFieldException ex) {
            System.out.println("Error removing world from bukkit master list: " + ex.getMessage());
        }

        MinecraftServer ms = server.getServer();

        ms.worlds.remove(ms.worlds.indexOf(craftWorld.getHandle()));
    }

    public static boolean clearRegionFiles(String worldName) {
        HashMap regionfiles = (HashMap) RegionFileCache.a;

        try {
            for (Iterator<Object> iterator = regionfiles.entrySet().iterator(); iterator.hasNext(); ) {
                Map.Entry e = (Map.Entry) iterator.next();
                RegionFile file = (RegionFile) e.getValue();

                try {
                    file.c();
                    iterator.remove();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        } catch (Exception ex) {
            System.out.println("Exception while removing world reference for '" + worldName + "'!");
            ex.printStackTrace();
        }

        return true;
    }

    public static void quickChangeBlock(World world, Location l, int id, int data)
    {
        int x = l.getBlockX();
        int y = l.getBlockY();
        int z = l.getBlockZ();

        Chunk chunk = world.getChunkAt(x >> 4, z >> 4);
        net.minecraft.server.v1_8_R3.Chunk c = ((CraftChunk) chunk).getHandle();

        //c.a(x & 0xF, y, z & 0xF, Block.getById(id), data);
        IBlockData blockData = CraftMagicNumbers.getBlock(id).fromLegacyData(data);
        c.a(getBlockPos(x, y, z), blockData);
        ((CraftWorld) world).getHandle().notify(getBlockPos(x, y, z));
    }

    public static BlockPosition getBlockPos(int x, int y, int z)
    {
        return new BlockPosition(x, y, z);
    }



    public static class WorldData {
        private
        @Getter
        World world;
        private
        @Getter
        HashMap<DyeColor, List<Location>> dataPoints = Maps.newHashMap(); //wool with gold pressure plate
        private
        @Getter
        HashMap<DyeColor, List<Location>> teamSpawns = Maps.newHashMap(); //wool with iron pressure plate
        private
        @Getter
        HashMap<String, List<Location>> customLocs = Maps.newHashMap(); //wool with sign
        private
        @Getter
        int minX, minY, minZ, maxX, maxY, maxZ;
        private
        @Getter
        String mapName = "";


        public WorldData(World world) {
            this.world = world;
            File file = new File(world.getWorldFolder(), "data.yml");
            if (file.exists()) {
                try {
                    List<String> lines = Files.readAllLines(file.toPath());
                    for (int i = 0; i < lines.size(); i++) {
                        String line = lines.get(i);

                        if (line.trim().isEmpty()) continue;

                        if (line.contains("NAME")) mapName = line.replace("NAME: ", "");
                        else if (line.contains("MinX"))
                            minX = Integer.valueOf(line.split(Pattern.quote(":"))[1].trim());
                        else if (line.contains("MinZ"))
                            minZ = Integer.valueOf(line.split(Pattern.quote(":"))[1].trim());
                        else if (line.contains("MinY"))
                            minY = Integer.valueOf(line.split(Pattern.quote(":"))[1].trim());
                        else if (line.contains("MaxX"))
                            maxX = Integer.valueOf(line.split(Pattern.quote(":"))[1].trim());
                        else if (line.contains("MaxY"))
                            maxY = Integer.valueOf(line.split(Pattern.quote(":"))[1].trim());
                        else if (line.contains("MaxZ"))
                            maxZ = Integer.valueOf(line.split(Pattern.quote(":"))[1].trim());
                        else if (line.contains("TEAM")) {
                            String nextLine = lines.get(++i);
                            String locs = nextLine.replace("TEAM_SPAWNS: ", "").trim();
                            teamSpawns.put(DyeColor.valueOf(line.replace("TEAM: ", "").trim()), getLocations(locs, world));
                        } else if (line.contains("DATA")) {
                            String nextLine = lines.get(++i);
                            String locs = nextLine.replace("DATA_LOCS: ", "").trim();
                            dataPoints.put(DyeColor.valueOf(line.replace("DATA: ", "").trim()), getLocations(locs, world));
                        } else if (line.contains("CUSTOM")) {
                            String nextLine = lines.get(++i);
                            String locs = nextLine.replace("CUSTOM_LOCS: ", "").trim();
                            String msg = line.replace("CUSTOM:", "").trim();

                            customLocs.put(msg, getLocations(locs, world));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        }

        private List<Location> getLocations(String msg, World world) {
            String[] stuff = msg.split(Pattern.quote(":"));
            List<Location> locs = Lists.newArrayList();
            for (String s : stuff) {
                locs.add(UtilLocation.fromString(s, world));
            }
            return locs;
        }
    }

}