package me.adamtanana.core.util;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

import java.util.List;
import java.util.regex.Pattern;

public class UtilLocation {

    public static Location fromString(String s, World world) {
        String[] parts = s.split(Pattern.quote(","));
        int i = 0;
        return new Location(world == null ? Bukkit.getWorld(parts[i++]) : world, Float.valueOf(parts[i++]), Float.valueOf(parts[i++]), Float.valueOf(parts[i++]));
    }

    public static String toString(Location loc, boolean world) {
        return (world ? loc.getWorld().getName() + "," : "") + loc.getX() + "," + loc.getY() + "," + loc.getZ();
    }

    public static String toString(List<Location> locs, boolean world) {
        StringBuilder finalString = new StringBuilder("");

        for(Location l : locs) {
            finalString.append(toString(l, world) + ":");
        }
        if(finalString.length()>0)finalString.setLength(finalString.length() - 1);
        return finalString.toString();
    }

    public static Location lookAt(Location loc, Location lookAt, boolean yaw, boolean pitch) {
        loc = loc.clone();
        double x = lookAt.getX() - loc.getX(), y = lookAt.getY() - loc.getY(), z = lookAt.getZ() - loc.getZ();
        if (yaw) {
            if (x != 0) {
                float f = 0;
                if (x < 0) f = (float) (1.5 * Math.PI);
                else f = (float) (0.5 * Math.PI);

                loc.setYaw(f - (float) Math.atan(z / x));
            } else if (z < 0) loc.setYaw((float) Math.PI);

            loc.setYaw(-loc.getYaw() * 180f / (float) Math.PI);
        }

        if (pitch) {
            double xz = Math.sqrt(Math.pow(x, 2) + Math.pow(z, 2));
            loc.setPitch((float) -Math.atan(y / xz));
            loc.setPitch(loc.getPitch() * 180f / (float) Math.PI);
        }

        return loc;
    }


    public static boolean equal(Location a, Location b) {
        return a.getBlockX() == b.getBlockX() && a.getBlockY() == b.getBlockY() &&a.getBlockZ() == b.getBlockZ();
    }
}
