package me.adamtanana.core.player;

import com.google.common.collect.Lists;
import me.adamtanana.core.Core;
import me.adamtanana.core.command.Command;
import me.adamtanana.core.redis.JedisServer;
import me.adamtanana.core.redis.message.RestartMessage;
import me.adamtanana.core.util.C;
import me.adamtanana.core.util.UtilServer;
import org.bukkit.entity.Player;

import java.util.List;

public class RestartCommand extends Command{

    public RestartCommand() {
        super("restart", Lists.newArrayList(), 1, Rank.DEVELOPER);
    }

    @Override
    public void execute(Player player, List<String> args) {
        player.sendMessage(C.yellow + "Restarting server: " + C.gray + args.get(0));
        Core.getJedis().sendMessage(new RestartMessage(args.get(0)), args.get(0));
        UtilServer.runTaskLater(() -> Core.getJedis().sendMessage(new RestartMessage(args.get(0)), "bungee"), 100L); //5seconds

        if(args.get(0).equalsIgnoreCase("all") || args.get(0).equalsIgnoreCase(UtilServer.getServerName())) {
            UtilServer.restart();
        }
    }
}
