package me.adamtanana.core.player;

import com.google.common.collect.Lists;
import me.adamtanana.core.command.Command;
import me.adamtanana.core.util.C;
import me.adamtanana.core.util.CommonUtil;
import me.adamtanana.core.util.UtilText;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.List;

public class DataCommand extends Command {

    public DataCommand() {
        super("data", Lists.newArrayList(), 1, Rank.DEVELOPER);
    }

    @Override
    public void execute(Player player, List<String> args) {
        GlobalPlayer gp;
        if(Bukkit.getPlayer(args.get(0)) == null) gp =  CommonUtil.getGlobalPlayer(CommonUtil.forceFetchUUID(args.get(0)));
        else gp = CommonUtil.getGlobalPlayer(Bukkit.getPlayer((args.get(0))).getUniqueId());

        player.sendMessage(C.bold + "=================");
        player.sendMessage(C.yellow + "User data: " + C.reset + args.get(0));

        if(gp == null) {
            player.sendMessage(C.blue + "This player does not exist!");
        } else {
            player.sendMessage(C.darkAqua + "UUID: " + C.yellow + gp.getUniqueUserId().toString());
            player.sendMessage(C.darkAqua + "Known IPS: " + C.yellow + UtilText.join(gp.getKnownIps(), ", "));
            player.sendMessage(C.darkAqua + "Previous names on this server: " + C.yellow + UtilText.join(gp.getOldNames(), ", "));
            player.sendMessage(C.darkAqua + "Rank: " + C.yellow + gp.getRank().getName(false, false, false));
            player.sendMessage(C.darkAqua + "Last Server: " + C.yellow + gp.getLastServer());
            player.sendMessage(C.darkAqua + "Friends: " + C.yellow + gp.getFriends().size());
            player.sendMessage(C.darkAqua + "Gadgets: " + C.yellow + gp.getAllGadgets().size());
            player.sendMessage(C.darkAqua + "Points: " + C.yellow + gp.getPoints());
            player.sendMessage(C.darkAqua + "Coins: " + C.yellow + gp.getCoins());
            player.sendMessage(C.darkAqua + "Bans: " + C.yellow + gp.getBanEntries().size());
            player.sendMessage(C.darkAqua + "Mutes: " + C.yellow + gp.getMuteEntries().size());
            player.sendMessage(C.darkAqua + "Old violations: " + C.yellow +
                    (gp.getOldViolations().getBans().size() + gp.getOldViolations().getMutes().size()));
        }

        player.sendMessage(C.bold + "=================");
    }
}
