package me.adamtanana.core.player;

import me.adamtanana.core.Module;
import me.adamtanana.core.event.ProfileLoadedEvent;
import me.adamtanana.core.util.*;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.Timer;
import java.util.UUID;

public class PlayerHandler extends Module {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onJoin(AsyncPlayerPreLoginEvent event) {
        UtilServer.log("===== Started loading of Player =====");
        UUID id = event.getUniqueId();
        GlobalPlayer globalPlayer = CommonUtil.getGlobalPlayer(id);

        long whileLoop = System.currentTimeMillis();
        while(!globalPlayer.isLoaded()) {} //FORCE LOAD PLAYER
        UtilServer.log("==== While loop took " + (System.currentTimeMillis() - whileLoop) + " ms ====");

        long banCheck = System.currentTimeMillis();
        globalPlayer.getBanEntries().forEach(ban -> {
            if (ban.isKick() || ban.expired()) {
                globalPlayer.getBanEntries().remove(ban);
                globalPlayer.getOldViolations().addBan(ban);
            } else {
                event.setLoginResult(AsyncPlayerPreLoginEvent.Result.KICK_BANNED);
                event.setKickMessage(ban.getKickReason());
            }
        });
        globalPlayer.getMuteEntries().forEach(mute -> {
            if(!mute.isPermanent()) {
                if(mute.expired()) {
                    globalPlayer.getMuteEntries().remove(mute);
                    globalPlayer.getOldViolations().addMute(mute);
                }
            }
        });

        UtilServer.log("==== Ban/mute check took " + (System.currentTimeMillis() - banCheck) + " ms ====");

        if(globalPlayer.getBanEntries().size() > 0) {
            //Don't load player
        } else {
            UtilServer.runTaskLater(() -> {
                if (Bukkit.getPlayer(id) == null) return;

                ProfileLoadedEvent profile = new ProfileLoadedEvent(Bukkit.getPlayer(id), globalPlayer);
                UtilServer.callEvent(profile);
            }, 20L);
        }

    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onLoaded(ProfileLoadedEvent e) {
        Player p = Bukkit.getPlayer(e.getGp().getUniqueUserId());
        e.getGp().setLastName(p.getName());
        e.getGp().setLastServer(UtilServer.getServerName());
        e.getGp().setLastIp(p.getAddress().getHostString());

        p.setOp(e.getGp().getRank().has(Rank.ADMIN));
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onQuit(PlayerQuitEvent event) {
        CommonUtil.invalidateGlobalPlayer(event.getPlayer().getUniqueId());
    }
}
