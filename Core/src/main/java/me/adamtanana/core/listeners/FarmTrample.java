package me.adamtanana.core.listeners;

import me.adamtanana.core.Module;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class FarmTrample extends Module{

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        if(e.getAction() == Action.PHYSICAL) {
            Block b = e.getClickedBlock();
            if(b == null) return;
            if(b.getType() == Material.SOIL) {
                e.setUseInteractedBlock(org.bukkit.event.Event.Result.DENY);
                e.setCancelled(true);
                b.setTypeIdAndData(b.getTypeId(), b.getData(), true);
            }
        }
    }

}
