package me.adamtanana.core.listeners;

import me.adamtanana.core.Module;
import org.bukkit.GameMode;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.block.BlockPlaceEvent;

public class Blocks extends Module {

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        e.setCancelled(e.getPlayer().getGameMode() != GameMode.CREATIVE);
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e) {
        e.setCancelled(e.getPlayer().getGameMode() != GameMode.CREATIVE);
    }

    @EventHandler
    public void onBurn(BlockBurnEvent e) {
        e.setCancelled(true);
    }

}
