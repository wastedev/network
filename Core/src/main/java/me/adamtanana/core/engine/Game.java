package me.adamtanana.core.engine;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import gnu.trove.impl.hash.TByteByteHash;
import lombok.Getter;
import lombok.Setter;
import me.adamtanana.core.Core;
import me.adamtanana.core.Module;
import me.adamtanana.core.engine.damage.DamageHandler;
import me.adamtanana.core.engine.event.GameEndingEvent;
import me.adamtanana.core.engine.event.GameFinishEvent;
import me.adamtanana.core.engine.event.GameInitialisedEvent;
import me.adamtanana.core.engine.player.GamePlayer;
import me.adamtanana.core.engine.player.PlayerState;
import me.adamtanana.core.engine.team.Team;
import me.adamtanana.core.engine.timer.GameTimer;
import me.adamtanana.core.util.*;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;

import java.util.*;
import java.util.stream.Collectors;

public abstract class Game implements IGame {
    @Getter private HashMap<UUID, List<GamePoints>> gamePoints = Maps.newHashMap();
    @Getter private List<Module> modules = Lists.newArrayList();
    @Getter private List<GamePlayer> gamePlayers = Lists.newArrayList();
    @Getter private List<Team> teams = Lists.newArrayList();

    @Getter private String[] winners = new String[3];
    @Getter private GameType gameType;
    @Getter private int min, max;
    @Getter private String[] description;
    @Getter private boolean initialised = false;
    @Getter private boolean live = false;

    @Getter private UtilWorld.WorldData gameWorld = null;

    public Game(GameType gameType, int[] slots, String[] description) {
        this.gameType = gameType;
        this.min = slots[0];
        this.max = slots[1];
        this.description = description;

        addTeam(new Team());
    }

    public void setGameTimer(GameTimer timer) {
        enableModules(timer); //this registers the listener
    }

    @Override
    public final void initialise() {
        initialised = true;

        System.out.println("Attempting to load random " + gameType.name() + " map.");
        /** MAP STUFF **/
        if(UtilWorld.hasMaps(gameType)) {
            UtilWorld.loadRandomWorld(gameType, (worldData) -> {
                gameWorld = worldData;
                System.out.println("Loaded map: " + gameWorld.getMapName());
            });
        } else {
            Bukkit.broadcastMessage(C.bold + "NO GAME MAPS FOUND!!!");
            end();
            initialised = false;
        }

        UtilServer.runTask(() -> UtilServer.callEvent(new GameInitialisedEvent(this)));
    }

    public abstract void start();
    public abstract void finish();
    public abstract void teleport(List<Team> teams);
    public abstract void register();

    public abstract GameSetting getGameSetting();

    @Override
    public final void end() {
        if(!live) return;
        if(!initialised) return;
        initialised = false;
        setLive(false);
        EngineHandler.setGameState(GameState.ENDING);

        Module timerModule = getModule(GameTimer.class);
        if(timerModule != null) {
            timerModule.disable();
            HandlerList.unregisterAll(timerModule);
            modules.remove(timerModule);
        }

        UtilServer.runTaskLater(() -> {
            if(!getLivePlayers().isEmpty()) {
                winners[0] = getLivePlayers().get(0).getPlayer().getName();
                finish(); //Can reset winners in here if needed

                //TODO: Get winner
                Bukkit.broadcastMessage("");
                Bukkit.broadcastMessage(C.aqua + "▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀");
                Bukkit.broadcastMessage(C.gold + C.bold + "Game results");
                Bukkit.broadcastMessage("");
                if (winners[0] != null) Bukkit.broadcastMessage(C.red + C.bold + "1st " + C.white + winners[0]);
                else Bukkit.broadcastMessage(C.red + C.bold + "No winners");
                if (winners[1] != null) Bukkit.broadcastMessage(C.yellow + C.bold + "2nd " + C.white + winners[1]);
                if (winners[2] != null) Bukkit.broadcastMessage(C.green + C.bold + "3rd " + C.white + winners[2]);
                Bukkit.broadcastMessage(C.aqua + "▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄");
                Bukkit.broadcastMessage("");
            }

            //Remove all "Winners"
            for (int i = 0; i < 3; i++) winners[i] = null;
        }, 1);

        HashMap<UUID, List<GamePoints>> tempPoints = (HashMap<UUID, List<GamePoints>>) gamePoints.clone();
        UtilServer.runTaskLater(() -> {
            tempPoints.keySet().stream().filter(uuid -> Bukkit.getPlayer(uuid) != null).forEach(uuid -> {
                Player player = Bukkit.getPlayer(uuid);
                player.sendMessage("");
                player.sendMessage(C.aqua + "▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀");
                player.sendMessage(C.gold + C.bold + "Game results");
                player.sendMessage("");
                for (GamePoints gp : tempPoints.get(uuid))
                    player.sendMessage(C.green + "+" + gp.getPoints() + " Points " + C.white + "- " + C.darkAqua + gp.getReason());
                player.sendMessage(C.aqua + "▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄");
                player.sendMessage("");
            });
        }, 60);

        UtilServer.log("World unloading...");
        gameWorld.getWorld().getEntities().stream().filter(entity -> !(entity instanceof Player)).forEach(Entity::remove);

        //5 seconds, call GameEndingEvent
        UtilServer.runTaskLater(() -> {
            UtilServer.callEvent(new GameEndingEvent(this));

            //2 seconds, Delete world & end game.
            UtilServer.runTaskLater(() -> UtilWorld.unloadWorld(gameWorld.getWorld().getName(), false, aBoolean -> {
                if (!aBoolean) {
                    UtilServer.log("World failed to delete.");
                } else {
                    UtilServer.log("World deleted");
                    UtilServer.runTask(() -> {
                        UtilServer.callEvent(new GameFinishEvent(this));
                        for (Module m : modules) {
                            HandlerList.unregisterAll(m);
                            m.disable();
                        }
                        modules.clear();
                    });
                }
            }), 40);
        }, 100);
    }

    @Override
    public void testEnd() {
        if(getLivePlayers().size() <= 1) end();
    }

    public void addTeam(Team team) {
        teams.add(team);
    }

    public GamePlayer addGamePlayer(Player player) {
        GamePlayer gamePlayer = new GamePlayer(player.getUniqueId());
        gamePlayers.add(gamePlayer);
        return gamePlayer;
    }


    public void enableModules(Module... mdls) {
        modules.addAll(Arrays.asList(mdls));

        if(EngineHandler.getGameState() == GameState.RUNNING) {
            for (Module m : mdls) {
                UtilServer.registerListener(m);
                m.enable();
            }
        }
    }

    public <T extends Module> T getModule(Class<T> clazz) {
        Optional<Module> found = modules.stream().filter(c -> c.getClass().equals(clazz)).findFirst();
        if (found.isPresent()) return clazz.cast(found.get());
        return null;
    }

    public List<Player> getLivePlayers() {
        List<Player> result = Lists.newArrayList();

        getGamePlayers().stream().filter(g -> g.getPlayerState() == PlayerState.PLAYING).forEach(p -> result.add(p.getPlayer()));
        return result;
    }

    public GamePlayer getGamePlayer(Player player) {
        for(GamePlayer gamePlayer : gamePlayers) {
            if(gamePlayer.getUuid().equals(player.getUniqueId())) {
                return gamePlayer;
            }
        }

        return null;
    }

    public List<GamePlayer> getOnlineGamePlayers() {
        return gamePlayers.stream().filter(gamePlayer -> gamePlayer.getPlayer() != null).collect(Collectors.toList());
    }

    public void removeGamePlayer(Player player) {
        gamePlayers.remove(getGamePlayer(player));
    }

    public void setSpectator(Player player) {
        getGamePlayer(player).setPlayerState(PlayerState.SPECTATING);
        player.setGameMode(GameMode.SPECTATOR);
    }

    public void doTeams() {
        Team team = getTeams().get(0);
        getGamePlayers().forEach(gamePlayer -> team.addPlayer(gamePlayer));
    }

    public void joinTeam(GamePlayer gamePlayer, Team team) {
        if(teams.size() == 1) {
            if(team.hasPlayer(gamePlayer.getUuid())) return;
            team.addPlayer(gamePlayer);
        } else {
            if(team.hasPlayer(gamePlayer.getUuid())) return;
            for(Team t : teams) {
                if(t.equals(team)) continue;
                t.removePlayer(gamePlayer.getUuid());
            }

            team.addPlayer(gamePlayer);
        }
    }

    public Team getPlayerTeam(Player player) {
        for (Team team : teams) {
            for (GamePlayer gamePlayer : team.getGamePlayers()) {
                if (!gamePlayer.getUuid().equals(player.getUniqueId())) continue;
                return team;
            }
        }

        return null;
    }

    public void setLive(boolean live) {
        this.live = live;
    }

    public GameOptions getGameOptions() {
        return getModule(GameOptions.class);
    }

    public void addPoints(UUID uuid, String reason, int amount) {
//        if(!this.gamePoints.containsKey(uuid)) this.gamePoints.put(uuid, Lists.newArrayList());
        CommonUtil.getGlobalPlayer(uuid).addPoints(amount);

        for(GamePoints points : gamePoints.get(uuid)) {
            if(!points.getReason().equalsIgnoreCase(reason)) continue;
            points.setPoints(points.getPoints()+amount);
            return;
        }

        gamePoints.get(uuid).add(new GamePoints(reason, amount));
    }

    public void addPoints(GamePlayer gamePlayer, String reason, int amount) {
        addPoints(gamePlayer.getUuid(), reason, amount);
    }
}
