package me.adamtanana.core.engine.update;

public enum UpdateCause {
    PLAYER_JOIN, PLAYER_LEAVE, PLAYER_DEATH, MAP,
}
