package me.adamtanana.core.engine;

import lombok.Getter;
import lombok.Setter;
import me.adamtanana.core.Module;
import me.adamtanana.core.engine.damage.DamageEvent;
import org.bukkit.Bukkit;
import org.bukkit.WeatherType;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

@Getter
@Setter
public class GameOptions extends Module{

    private boolean pvpDamage, pveDamage, evpDamage, fallDamage;
    private boolean healthRegen, foodRegen;

    private int maxHealth = 20, startingFoodLevel = 20;

    private String world = "world";
    private WeatherType worldWeather = WeatherType.CLEAR;
    private int worldTime = 12200;//Not too bright, not too dark.
    private boolean lockWorldTime = true, lockWorldWeather = true;

    @Override
    public void enable() {
        World w = Bukkit.getWorld(world);
        if(w != null) setWorldSettings(w);

        Bukkit.getOnlinePlayers().forEach(player -> {
            player.setMaxHealth(maxHealth);
            player.setHealth(player.getMaxHealth());
            player.setFoodLevel(startingFoodLevel);
        });
    }

    private void setWorldSettings(World w) {
        w.setTime(worldTime);
        if (lockWorldTime) w.setGameRuleValue("doDaylightCycle", "false");
        w.setStorm(worldWeather == WeatherType.DOWNFALL);
        w.setWeatherDuration(9999999);
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        player.setMaxHealth(maxHealth);
        player.setHealth(player.getMaxHealth());
        player.setFoodLevel(startingFoodLevel);
    }


    @EventHandler
    public void onDamage(DamageEvent event) {
        Entity attacker = event.getDamager(), target = event.getVictim();
        if(attacker != null) {
            boolean isAttackerPlayer = attacker instanceof Player, isTargetPlayer = target instanceof Player;

            //Player vs Player
            if ((isAttackerPlayer && isTargetPlayer) && !pvpDamage) {
                event.setCancelled(true);
            }

            //Player vs Entity
            if ((isAttackerPlayer && !isTargetPlayer) && !pveDamage) {
                event.setCancelled(true);
            }

            //Entity vs Player
            if ((!isAttackerPlayer && isTargetPlayer) && !evpDamage) {
                event.setCancelled(true);
            }
        }

        if(event.getCause() == EntityDamageEvent.DamageCause.FALL) {
            event.setCancelled(!fallDamage);
        }
    }

    @EventHandler
    public void onHealthRegen(EntityRegainHealthEvent event) {
        if(!healthRegen) event.setCancelled(true);
    }

    @EventHandler
    public void onHungerDrain(FoodLevelChangeEvent event) {
        if(!foodRegen) event.setCancelled(true);
    }

    @EventHandler
    public void onWeatherChange(WeatherChangeEvent event) {
        //Check if the world is the world we specified.
        if(!event.getWorld().getName().equalsIgnoreCase(world)) return;

        if(lockWorldWeather) event.setCancelled(true);
    }
}
