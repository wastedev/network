package me.adamtanana.core.engine.player;

public enum PlayerState {
    PLAYING, SPECTATING
}
