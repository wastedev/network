package me.adamtanana.core.engine;

import me.adamtanana.core.engine.player.GamePlayer;
import me.adamtanana.core.engine.player.PlayerState;
import me.adamtanana.core.engine.team.Team;
import org.bukkit.entity.Player;

public abstract class TeamGame extends Game {

    public TeamGame(GameType gameType, int[] slots, String[] description, Team[] team) {
        super(gameType, slots, description);

        getTeams().clear();
        for(Team t : team) addTeam(t);
    }

    @Override
    public void testEnd() {
        int total = 0;
        for(Team t : getTeams()) {
            if(getLivePlayers().size() != 0) total++;
        }
        if(total<=1) end();
    }

    @Override
    public void doTeams() {
        for(Player p : getLivePlayers()) {
            getLowestTeam().addPlayer(new GamePlayer(p.getUniqueId()));
        }
    }

    public Team getLowestTeam() {
        Team t = getTeams().get(0);

        for(Team team : getTeams()) {
            if(t.getPlayers().size() < t.getPlayers().size())
                t = team;
        }

        return t;
    }
}
