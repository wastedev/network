package me.adamtanana.core.engine.timer;

import me.adamtanana.core.Module;
import me.adamtanana.core.engine.Game;
import me.adamtanana.core.engine.event.GameStartingEvent;
import me.adamtanana.core.event.UpdateEvent;
import me.adamtanana.core.event.UpdateType;
import me.adamtanana.core.util.C;
import me.adamtanana.core.util.UtilServer;
import me.adamtanana.core.util.UtilText;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;

public class GameTimer extends Module {
    private Game game;
    private int hours, mins, secs;
    private long combined;
    private boolean dark = true, finished;

    public GameTimer(Game game, int hours, int minutes, int seconds) {
        this.game = game;
        this.hours = hours;
        this.mins = minutes;
        this.secs = seconds;

        this.combined = ((hours * (60 * 60 * 1000)) + (minutes * (60 * 1000)) + (seconds * 1000));
    }

    @EventHandler
    public void onTimeElapsed(UpdateEvent event) {
        if(event.getType() != UpdateType.SEC) return;
        if(finished) return;

        reduce();
        Bukkit.getOnlinePlayers().forEach(player -> UtilText.sendActionBar(player, getAsTime()));

        if(hasElapsed()) {
            UtilServer.log("GameTimer elapsed, ending game");
            finished = true;
            game.end();
        }
    }

    public int getHours() {
        return hours;
    }

    public int getMins() {
        return mins;
    }

    public int getSecs() {
        return secs;
    }

    public String getStringHours() {
        if(String.valueOf(hours).length() < 2) {
            return "0" + hours;
        }

        return ""+hours;
    }

    public String getStringMins() {
        if(String.valueOf(mins).length() < 2) {
            return "0" + mins;
        }

        return ""+mins;
    }

    public String getStringSecs() {
        if(String.valueOf(secs).length() < 2) {
            return "0" + secs;
        }

        return ""+secs;
    }

    public String getAsTime() {
        dark = !dark;
        String color;
        if(hours <= 0 && mins <= 0) {
            if(secs > 30) {
                color = (dark ? C.yellow : C.gold) + C.bold;
                return color + getStringHours() + C.gray + ":" + color + getStringMins() + C.gray + ":" + color + getStringSecs();
            } else {
                color = (dark ? C.red : C.darkRed) + C.bold;
                return color + getStringHours() + C.gray + ":" + color + getStringMins() + C.gray + ":" + color + getStringSecs();
            }
        }

        color = (dark ? C.green : C.darkGreen) + C.bold;
        return color + getStringHours() + C.gray + ":" + color + getStringMins() + C.gray + ":" + color + getStringSecs();
    }

    private void reduce() {
        if(secs > 0) {
            secs--;
        } else {
            if(mins > 0) {
                mins--;
                secs = 59;
            } else {
                if(hours > 0) {
                    hours--;
                    mins = 59;
                    secs = 59;
                }
            }
        }
    }

    public boolean hasElapsed() {
        return (hours <= 0 && mins <= 0 && secs <= 0);
    }
}
