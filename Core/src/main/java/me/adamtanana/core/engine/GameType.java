package me.adamtanana.core.engine;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum GameType {
    Lobby("Lobby"), //For lobby maps dont init this game ever
    ESCAPE("Escape");

    private String name;
}
