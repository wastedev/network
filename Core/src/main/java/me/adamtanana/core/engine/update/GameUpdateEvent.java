package me.adamtanana.core.engine.update;

import lombok.AllArgsConstructor;
import lombok.Getter;
import me.adamtanana.core.engine.Game;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

@AllArgsConstructor
public class GameUpdateEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    @Getter private Game game;
    @Getter private UpdateCause cause;

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
