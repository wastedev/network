package me.adamtanana.core.engine.countdown;

import lombok.Getter;
import me.adamtanana.core.engine.Game;
import me.adamtanana.core.util.UtilServer;
import me.adamtanana.core.util.UtilText;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.HashMap;
import java.util.Map;

public class Countdown {

    private Map<Integer, String> modules;
    private CountdownDisplay[] countdownDisplays;
    private int start, tick, id;
    @Getter private boolean running = false;

    private BukkitTask task;
    private Game game;

    public Countdown(Game game, int id, int start, CountdownDisplay... countdownDisplays) {
        this.game = game;
        this.id = id;
        this.start = start;
        this.tick = start;
        this.countdownDisplays = countdownDisplays;
        this.modules = new HashMap<>();
    }

    public void addDisplay(int tick, String text) {
        modules.put(tick, text.replace("{0}", "" + tick));
    }

    public void removeDisplay(int tick) {
        if(modules.containsKey(tick)) modules.remove(tick);
    }

    public void run() {
        if(task != null) return;
        running = true;
        task = new BukkitRunnable() {
            public void run() {
                if(modules.containsKey(tick)) {
                    String text = modules.get(tick).replace("{0}", tick+"");
                    for(CountdownDisplay cd : countdownDisplays) {
                        switch (cd) {
                            case CHAT:
                                game.getOnlineGamePlayers().forEach(gamePlayer -> gamePlayer.getPlayer().sendMessage(text));
                                break;

                            case BOSS_BAR:
                                //TODO
                                break;

                            case ACTION_BAR:
                                game.getOnlineGamePlayers().forEach(gamePlayer -> UtilText.sendActionBar(gamePlayer.getPlayer(), text));
                                break;

                            case TITLE:
                                game.getOnlineGamePlayers().forEach(gamePlayer -> UtilText.textMiddle(gamePlayer.getPlayer(), text, " "));
                                break;

                            case SUB_TITLE:
                                game.getOnlineGamePlayers().forEach(gamePlayer -> UtilText.textMiddle(gamePlayer.getPlayer(), " ", text));
                                break;
                        }
                    }
                }

                if(tick <= 0) {
                    UtilServer.callEvent(new CountdownEvent(game, id));
                    this.cancel();
                    return;
                }

                tick--;
            }
        }.runTaskTimer(UtilServer.getPlugin(), 0, 20);
    }

    public void forceStop() {
        if(task == null) return;
        task.cancel();
        tick = start;
        running = false;
    }
}
