package me.adamtanana.core.engine.event;

import lombok.Getter;
import me.adamtanana.core.engine.Game;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class GameEndingEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    @Getter private Game game;

    public GameEndingEvent(Game game) {
        this.game = game;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
