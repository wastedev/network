package me.adamtanana.core.engine.countdown;

import lombok.Getter;
import me.adamtanana.core.engine.Game;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class CountdownEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    @Getter private Game game;
    @Getter private int id;

    public CountdownEvent(Game game, int id) {
        this.game = game;
        this.id = id;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
