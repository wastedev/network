package me.adamtanana.core.engine;

import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.Setter;
import me.adamtanana.core.Core;
import me.adamtanana.core.Module;
import me.adamtanana.core.engine.countdown.Countdown;
import me.adamtanana.core.engine.countdown.CountdownDisplay;
import me.adamtanana.core.engine.countdown.CountdownEvent;
import me.adamtanana.core.engine.countdown.GameCountdown;
import me.adamtanana.core.engine.damage.DamageHandler;
import me.adamtanana.core.engine.event.*;
import me.adamtanana.core.engine.player.GamePlayer;
import me.adamtanana.core.engine.player.PlayerState;
import me.adamtanana.core.engine.team.Team;
import me.adamtanana.core.engine.update.GameUpdateEvent;
import me.adamtanana.core.engine.update.UpdateCause;
import me.adamtanana.core.event.UpdateEvent;
import me.adamtanana.core.event.UpdateType;
import me.adamtanana.core.listeners.Damage;
import me.adamtanana.core.player.GlobalPlayer;
import me.adamtanana.core.player.Rank;
import me.adamtanana.core.player.StatEntry;
import me.adamtanana.core.redis.message.UpdateGameData;
import me.adamtanana.core.util.C;
import me.adamtanana.core.util.CommonUtil;
import me.adamtanana.core.util.UtilServer;
import me.adamtanana.core.util.UtilWorld;
import net.minecraft.server.v1_8_R3.PacketPlayOutEntityTeleport;
import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.GameMode;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.List;
import java.util.Random;

public class EngineHandler extends Module {

    private List<Module> lobbyModules;
    private List<Game> games;
    private Countdown countdown = null;
    private UtilWorld.WorldData lobbyWorld;
    @Getter @Setter private static GameState gameState = GameState.LOBBY;

    public EngineHandler(Game... gamesArray) {
        this.games = Lists.newArrayList();
        for (Game g : gamesArray) {
            this.games.add(g);
        }
    }

    @Override
    public void enable() {
        this.lobbyModules = Lists.newArrayList();
        GameOptions options = new GameOptions();
        options.setWorld("GameLobby");
        options.setWorldTime(0);
        this.lobbyModules.add(options);
        this.lobbyModules.add(new DamageHandler());

        UtilWorld.loadWorld("Lobby", "GameLobby", worldData -> this.lobbyWorld = worldData);

        this.getGame().initialise();
    }

    @Override
    public void disable() {
        UtilWorld.unloadWorldSync("GameLobby");
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        event.setJoinMessage(null);
        Player player = event.getPlayer();
        Game game = getGame();
        GamePlayer gamePlayer = game.addGamePlayer(event.getPlayer());
        if (game instanceof TeamGame) game.joinTeam(gamePlayer, getGame().getTeams().get(new Random().nextInt(getGame().getTeams().size())));
        else game.joinTeam(gamePlayer, game.getTeams().get(0));

        if (gameState != GameState.RUNNING) player.teleport(lobbyWorld.getDataPoints().get(DyeColor.BLACK).get(0));
        else player.teleport(lobbyWorld.getDataPoints().get(DyeColor.BLACK).get(0));

        if (gameState == GameState.LOBBY) {
            Bukkit.broadcastMessage(C.yellow + event.getPlayer().getName() + C.darkAqua + " has joined (" + game.getGamePlayers().size() + "/" + game.getMax() + ")");
            player.setGameMode(GameMode.ADVENTURE);

            gamePlayer.setPlayerState(PlayerState.PLAYING);

            if(game.getGamePlayers().size() >= game.getMin()) {
                if (countdown == null) {
                    UtilServer.log("Countdown null, starting new countdown.");
                    countdown = new GameCountdown(game, 1, true, C.yellow, C.gold);
                    countdown.run();
                } else {
                    UtilServer.log("Countdown is not null");
                }
            }
        } else {
            gamePlayer.setPlayerState(PlayerState.SPECTATING);
        }

        GameScoreboard.setLobbyBoard(game, player);
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        GamePlayer gamePlayer = getGame().getGamePlayer(event.getPlayer());
        if (gamePlayer.getPlayerState() == PlayerState.PLAYING) {
            getGame().setSpectator(event.getPlayer());
        }

        Team team = getGame().getPlayerTeam(event.getPlayer());
        if (team != null) team.removePlayer(gamePlayer.getUuid());
        getGame().removeGamePlayer(event.getPlayer());

        if (getGame().isLive()) {
            if (getGame().getLivePlayers().size() == 2) getGame().getWinners()[2] = event.getPlayer().getName();
            else if (getGame().getLivePlayers().size() == 1) getGame().getWinners()[1] = event.getPlayer().getName();
        }

        if (gameState == GameState.LOBBY && getGame().getGamePlayers().size() < getGame().getMin()) {
            if (countdown != null && countdown.isRunning()) {
                UtilServer.log("Countdown is not null, force stopping");
                countdown.forceStop();
                countdown = null;
            }
        }

        UtilServer.callEvent(new GameUpdateEvent(getGame(), UpdateCause.PLAYER_LEAVE));
    }

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent event) {
        //event.setDeathMessage(C.aqua + event.getEntity().getName() + C.darkAqua + " has died.");//TODO: Remove once combat log is finished.
        GamePlayer gamePlayer = getGame().getGamePlayer(event.getEntity());
        if (gamePlayer.getPlayerState() != PlayerState.PLAYING) return;

        event.getEntity().setHealth(event.getEntity().getMaxHealth());
        getGame().setSpectator(event.getEntity());

        if (getGame().getLivePlayers().size() == 2) {
            getGame().getWinners()[2] = event.getEntity().getName();
        } else if (getGame().getLivePlayers().size() == 1) {
            getGame().getWinners()[1] = event.getEntity().getName();
        }

        UtilServer.callEvent(new GameUpdateEvent(getGame(), UpdateCause.PLAYER_DEATH));

        GlobalPlayer globalPlayer = CommonUtil.getGlobalPlayer(gamePlayer.getUuid());
        globalPlayer.addStat(new StatEntry(getGame().getGameType().name(), "Deaths", 1));
    }

    @EventHandler
    public void onGameInitialise(GameInitialisedEvent event) {
        for (Module m : lobbyModules) {
            UtilServer.registerListener(m);
            m.enable();
        }

        if(gameState != GameState.LOBBY) setGameState(GameState.LOBBY);
        Game game = event.getGame();
        game.getGamePlayers().clear();

        Bukkit.getOnlinePlayers().forEach(player -> {
            GamePlayer gamePlayer = getGame().addGamePlayer(player);
            if (getGame() instanceof TeamGame) getGame().joinTeam(gamePlayer, getGame().getTeams().get(new Random().nextInt(getGame().getTeams().size())));
            else getGame().joinTeam(gamePlayer, getGame().getTeams().get(0));

            GameScoreboard.setLobbyBoard(getGame(), player);
        });

        if (countdown == null && game.getGamePlayers().size() >= game.getMin()) {
            countdown = new GameCountdown(game, 1, true, C.yellow, C.gold);
            countdown.run();
        }
    }

    @EventHandler
    public void onCountdownFinish(CountdownEvent event) {
        if (event.getId() == 1) {
            if(getGame().getLivePlayers().size() < getGame().getMin()) {
                Bukkit.broadcastMessage(C.red + "Game needs more players to start.");
                countdown = null;
                return;
            }

            UtilServer.callEvent(new GameStartingEvent(event.getGame()));
            countdown = null;
        } else if (event.getId() == 2) {
            getGame().register();
            getGame().enableModules(new DamageHandler());
            for (Module m : getGame().getModules()) {
                UtilServer.registerListener(m);
                m.enable();
            }

            getGame().getGamePoints().clear();
            getGame().getLivePlayers().forEach(gamePlayer -> getGame().getGamePoints().put(gamePlayer.getUniqueId(), Lists.newArrayList()));

            event.getGame().start();
            setGameState(GameState.RUNNING);

            getGame().setLive(true);
            UtilServer.callEvent(new GameStartEvent(getGame()));

            //Disable GameLobby Modules.
            for (Module m : lobbyModules) {
                UtilServer.unregisterListener(m);
                m.disable();
            }
        }
    }

    @EventHandler
    public void onGamePreStart(GameStartingEvent e) {
        getGame().teleport(getGame().getTeams());

        Countdown countdown = new Countdown(getGame(), 2, 6, CountdownDisplay.TITLE);
        countdown.addDisplay(5, C.darkRed + "5");
        countdown.addDisplay(4, C.red + "4");
        countdown.addDisplay(3, C.gold + "3");
        countdown.addDisplay(2, C.yellow + "2");
        countdown.addDisplay(1, C.green + "1");
        countdown.addDisplay(0, " ");
        countdown.run();

        Bukkit.broadcastMessage("");
        Bukkit.broadcastMessage(C.aqua + "▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀");
        Bukkit.broadcastMessage(C.gold + C.bold + e.getGame().getGameType().getName());
        Bukkit.broadcastMessage("");
        for (String s : e.getGame().getDescription()) Bukkit.broadcastMessage(C.yellow + s);
        Bukkit.broadcastMessage("");
        Bukkit.broadcastMessage(C.bold + "Map name: " + C.gold + C.bold + e.getGame().getGameWorld().getMapName());
        Bukkit.broadcastMessage(C.aqua + "▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄");
        Bukkit.broadcastMessage("");
    }

    @EventHandler
    public void onGameFinish(GameFinishEvent event) {
        if(gameState != GameState.ENDING) setGameState(GameState.ENDING);
        getGame().getGamePlayers().clear();
        getGame().setLive(false);
        games.add(games.remove(0)); // Put at top of list
        countdown = null;
        games.get(0).initialise();
    }

    @EventHandler
    public void onGameUpdate(GameUpdateEvent event) {
        Game game = event.getGame();

        if (game instanceof TeamGame) {

        } else {
            switch (event.getCause()) {
                case PLAYER_LEAVE:
                case PLAYER_DEATH:
                    game.testEnd();
                    break;
            }
        }
    }

    @EventHandler
    public void onGameEnding(GameEndingEvent event) {
        Bukkit.getOnlinePlayers().forEach(player -> {
            player.teleport(lobbyWorld.getDataPoints().get(DyeColor.BLACK).get(0));
            player.setGameMode(GameMode.ADVENTURE);
        });
    }

    @EventHandler
    public void onWorldEnter(PlayerChangedWorldEvent event) {
        UtilServer.runTaskLater(() -> {
            UtilServer.broadcastPacket(new PacketPlayOutEntityTeleport(((CraftPlayer) event.getPlayer()).getHandle()));
            Bukkit.getOnlinePlayers().forEach(p -> {
                event.getPlayer().showPlayer(p);
                p.showPlayer(event.getPlayer());
            });
        }, 20);
    }

    @EventHandler
    public void onPlayerChat(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();
        GlobalPlayer globalPlayer = CommonUtil.getGlobalPlayer(player.getUniqueId());
        if (globalPlayer == null) {
            UtilServer.log("globalPlayer null");
            return;
        }

        Rank rank = globalPlayer.getRank();
        if (rank == null) {
            UtilServer.log("rank null");
            return;
        }

        Team team = getGame().getPlayerTeam(player);
        if (team == null) {
            UtilServer.log("team null");
            return;
        }

        event.setFormat(rank.getName(true, true, true) + " " + team.getColor() + player.getName() + C.white + " " + event.getMessage());
    }

    @EventHandler
    public void updateBungee(UpdateEvent e) {
        if(e.getType() != UpdateType.SEC) {
            ((Core) UtilServer.getPlugin()).getJedis().sendMessage(new UpdateGameData(getGame().getMax(), getGameState() != GameState.LOBBY), "bungee");
        }
    }

    public Game getGame() {
        return games.get(0);
    }
}
