package me.adamtanana.core.engine.damage;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.AllArgsConstructor;
import lombok.Getter;
import me.adamtanana.core.Module;
import me.adamtanana.core.listeners.Damage;
import me.adamtanana.core.util.C;
import me.adamtanana.core.util.UtilServer;
import me.adamtanana.core.util.UtilTime;
import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class DamageHandler extends Module {
    public HashMap<UUID, List<DamageLog>> damageLog = Maps.newHashMap();

    @Override
    public void enable() {
        damageLog.clear();
        Bukkit.getOnlinePlayers().forEach(p -> damageLog.put(p.getUniqueId(), Lists.newArrayList()));
        UtilServer.log("Enabled DAMAGE HANDLER");
    }

    @Override
    public void disable() {
        damageLog.clear();
    }

    @EventHandler
    public void onDamage(EntityDamageEvent event) {
        if (!(event.getEntity() instanceof Player)) return;
        if(!damageLog.containsKey(event.getEntity().getUniqueId())) return;
        DamageEvent damageEvent = new DamageEvent((Player) event.getEntity(), null, event.getDamage(), event.getCause());
        UtilServer.callEvent(damageEvent);

        if(damageEvent.isCancelled()) {
            event.setCancelled(true);
            return;
        }

        double damage = event.getDamage();
        damageLog.get(event.getEntity().getUniqueId()).add(0, new DamageLog(damage, System.currentTimeMillis(), event.getCause().name().replace("_", "")));

        for (DamageEvent.DamageMod mod : damageEvent.getModifies()) {
            damageLog.get(event.getEntity().getUniqueId()).add(0, new DamageLog(mod.getDamage(), System.currentTimeMillis(), mod.getReason()));
            damage += mod.getDamage(); //Can be negative
        }

        event.setDamage(damage);

    }

    @EventHandler
    public void onDamage(EntityDamageByEntityEvent event) {
        if (!(event.getEntity() instanceof Player)) return;
        if(!damageLog.containsKey(event.getEntity().getUniqueId())) return;
        String cause = event.getCause().name().replaceAll("_", "");
        DamageEvent damageEvent = new DamageEvent((Player) event.getEntity(), event.getDamager(), event.getDamage(), event.getCause());
        UtilServer.callEvent(damageEvent);

        if(damageEvent.isCancelled()) {
            event.setCancelled(true);
            return;
        }

        double damage = event.getDamage();
        damageLog.get(event.getEntity().getUniqueId()).add(0, new DamageLog(damage, System.currentTimeMillis(), cause));

        for (DamageEvent.DamageMod mod : damageEvent.getModifies()) {
            damageLog.get(event.getEntity().getUniqueId()).add(0, new DamageLog(mod.getDamage(), System.currentTimeMillis(), mod.getReason()));
            damage += mod.getDamage(); //Can be negative
        }

        event.setDamage(damage);
    }

    @EventHandler
    public void playerDeath(PlayerDeathEvent event) {
        event.setDeathMessage(null);
        Player p = event.getEntity();
        if(!damageLog.containsKey(p.getUniqueId())) return;
        List<DamageLog> damages = damageLog.get(p.getUniqueId());
        if(event.getEntity().getKiller() == null) {
            if(damages.size() == 0) {
                Bukkit.broadcastMessage(C.yellow + p.getName() + C.darkAqua + " died");
            } else {
                Bukkit.broadcastMessage(C.yellow + p.getName() + C.darkAqua + " was killed by " + damages.get(0).getDamageMessage());
            }
        } else {
            Bukkit.broadcastMessage(C.yellow + p.getName() + C.darkAqua + " was killed by " + C.yellow + p.getKiller().getName());
        }

        for(int i = 0; i < (damages.size() > 5 ? 5 : damages.size()); i++) {
            DamageLog log = damages.get(i);
            DecimalFormat format = new DecimalFormat("#.##");
            String ago = format.format((System.currentTimeMillis() - log.getTime()) / 1000f);
            p.sendMessage(C.yellow + (i+1) + C.darkAqua + " " + ago + "s ago: " + C.yellow + log.getDamage() + " dmg: " + C.darkAqua + log.getDamageMessage());
        }
    }

    public void damagePlayer(Player player, double damage, String cause) {
        if(!damageLog.containsKey(player.getUniqueId())) return;
        damageLog.get(player.getUniqueId()).add(0, new DamageLog(damage, System.currentTimeMillis(), cause));
        player.damage(damage);
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        damageLog.putIfAbsent(event.getPlayer().getUniqueId(), Lists.newArrayList());
    }

    @Getter
    @AllArgsConstructor
    private class DamageLog {
        private double damage;
        private long time;
        private String damageMessage;
    }
}
