package me.adamtanana.core.engine;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@AllArgsConstructor
public class GamePoints {
    private String reason;
    @Setter
    private int points;
}
