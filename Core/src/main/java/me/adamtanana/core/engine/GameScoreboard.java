package me.adamtanana.core.engine;

import me.adamtanana.core.Module;
import me.adamtanana.core.scoreboard.common.EntryBuilder;
import me.adamtanana.core.scoreboard.common.animate.FrameAnimatedString;
import me.adamtanana.core.scoreboard.common.animate.HighlightedString;
import me.adamtanana.core.scoreboard.type.ByteScoreboard;
import me.adamtanana.core.scoreboard.type.Entry;
import me.adamtanana.core.scoreboard.type.ScoreboardHandler;
import me.adamtanana.core.util.C;
import me.adamtanana.core.util.CommonUtil;
import me.adamtanana.core.util.UtilEntity;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.List;

public class GameScoreboard extends Module {

    @Override
    public void enable() {
    }

    public static ByteScoreboard setLobbyBoard(Game game, Player player) {
        ByteScoreboard scoreboard = new ByteScoreboard(player);
        scoreboard.setHandler(new ScoreboardHandler() {
            HighlightedString title = new HighlightedString(game.getGameType().getName(), "&f&l", "&c&l");

            @Override
            public String getTitle(Player player) {
                return title.next();
            }

            @Override
            public List<Entry> getEntries(Player player) {
                return new EntryBuilder()
                        .blank()
                        .next(C.red + C.bold + "Players: ")
                        .next(C.yellow + C.bold + game.getGamePlayers().size() + C.white + " / " + C.gold + C.bold + game.getMax())
                        .blank()
                        .next(C.yellow + C.bold + "Chosen Map:")
                        .next(game.getGameWorld() == null ? "loading" : game.getGameWorld().getMapName())
                        .blank()
                        .next(C.green + C.bold + "Selected Kit:")
                        .next(C.white + "null")
                        .blank()
//                        .next(C.aqua + C.bold + "Team:")
//                        .next(C.white + "'" + game.getPlayerTeam(player).getColor() + game.getPlayerTeam(player).getName() + C.white + "'")
//                        .blank()
                        .next(C.white + C.bold + "Game Type:")
                        .next(game.getGameSetting().getName())
                        .build();
            }

        }).setUpdateInterval(5).activate();

        Bukkit.getOnlinePlayers().forEach(p -> UtilEntity.setNameTag(p, CommonUtil.getGlobalPlayer(p.getUniqueId()).getRank().getName(true, true, true) + game.getPlayerTeam(player).getColor()));

        return scoreboard;
    }
}
