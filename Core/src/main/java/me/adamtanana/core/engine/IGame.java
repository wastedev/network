package me.adamtanana.core.engine;

interface IGame {
    void initialise();
    void end();
    void testEnd();
}
