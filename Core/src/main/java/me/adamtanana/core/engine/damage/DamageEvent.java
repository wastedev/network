package me.adamtanana.core.engine.damage;

import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.entity.Entity;
import org.bukkit.event.entity.EntityDamageEvent;

import java.util.List;

public class DamageEvent extends Event {
    private static final HandlerList handlers = new HandlerList();

    @Getter
    private Player victim;
    @Getter
    private Entity damager;
    @Getter
    private double damage;
    @Getter
    private EntityDamageEvent.DamageCause cause;
    @Getter
    @Setter
    private boolean cancelled = false;
    @Getter
    private List<DamageMod> modifies = Lists.newArrayList();

    public DamageEvent(Player victim, Entity damager, double damage, EntityDamageEvent.DamageCause cause) {
        this.victim = victim;
        this.damager = damager;
        this.damage = damage;
        this.cause = cause;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public void modify(DamageMod mod)  {
        modifies.add(mod);
    }

    @AllArgsConstructor
    @Getter
    public class DamageMod {
        private String reason;
        private double damage;
    }
}
