package me.adamtanana.core.engine.team;

import com.google.common.collect.Lists;
import lombok.Getter;
import me.adamtanana.core.engine.player.GamePlayer;
import me.adamtanana.core.engine.player.PlayerState;
import me.adamtanana.core.util.C;
import org.bukkit.DyeColor;
import org.bukkit.entity.Player;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Getter
public class Team {

    private String name, color;
    private DyeColor teamColor;
    private Set<GamePlayer> gamePlayers;

    public Team(String name, DyeColor teamColor, String color) {
        this.name = name;
        this.teamColor = teamColor;
        this.color = color;

        gamePlayers = new HashSet<>();
    }

    public Team() {
        this("Players", DyeColor.LIME, C.green);
    }

    public boolean hasPlayer(UUID uuid) {
        for(GamePlayer gamePlayer : gamePlayers) {
            if(gamePlayer.getUuid().equals(uuid)) return true;
        }
        return false;
    }

    public void removePlayer(UUID uuid) {
        GamePlayer found = null;
        for(GamePlayer gamePlayer : gamePlayers) {
            if(!gamePlayer.getUuid().equals(uuid)) continue;
            found = gamePlayer;
        }

        if(found == null) return;
        gamePlayers.remove(found);
    }

    public void addPlayer(GamePlayer gamePlayer) {
        gamePlayers.add(gamePlayer);
    }

    public List<Player> getPlayers() {
        List<Player> players = Lists.newArrayList();
        gamePlayers.stream().filter(g -> g.getPlayerState() == PlayerState.PLAYING).forEach(g -> players.add(g.getPlayer()));
        return players;
    }
}
