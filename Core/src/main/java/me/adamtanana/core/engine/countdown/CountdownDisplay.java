package me.adamtanana.core.engine.countdown;

public enum CountdownDisplay {
    CHAT, BOSS_BAR, ACTION_BAR, TITLE, SUB_TITLE
}
