package me.adamtanana.core.engine;

import lombok.AllArgsConstructor;
import lombok.Getter;
import me.adamtanana.core.util.C;

@Getter
@AllArgsConstructor
public enum GameSetting {
    NORMAL(C.aqua + "Standard"),
    CRAZY(C.red + "Crazy"),
    MEGA(C.yellow + "Mega"),
    TOURNAMENT(C.green + "Tournament"),
    EVENT(C.gold + "Event");

    private String name;
}
