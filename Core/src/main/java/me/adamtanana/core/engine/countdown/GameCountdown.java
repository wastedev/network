package me.adamtanana.core.engine.countdown;

import me.adamtanana.core.engine.Game;
import me.adamtanana.core.util.C;

public class GameCountdown extends Countdown {

    public GameCountdown(Game game, int id) {
        super(game, id, 30, CountdownDisplay.CHAT, CountdownDisplay.ACTION_BAR);

        addDisplay(30, "Game starting in {0} seconds.");
        addDisplay(15, "Game starting in {0} seconds.");
        addDisplay(10, "Game starting in {0} seconds.");
        addDisplay(5, "Game starting in {0} seconds.");
        addDisplay(4, "Game starting in {0} seconds.");
        addDisplay(3, "Game starting in {0} seconds.");
        addDisplay(2, "Game starting in {0} seconds.");
        addDisplay(1, "Game starting in {0} second.");
        addDisplay(0, "Game is starting!");
    }

    public GameCountdown(Game game, int id, boolean actionbar, String color1, String color2) {
        super(game, id, 30, CountdownDisplay.ACTION_BAR);

        for(int i = 1; i < 31; i++) {
            addDisplay(i, color1 + C.bold + "GAME STARTING IN " + color2 + C.bold + "{0} " + color1 + C.bold + "SECOND" + (i==1 ? "" : "S"));
        }
        addDisplay(0, color1 + C.bold + "Game is starting");
    }
}
