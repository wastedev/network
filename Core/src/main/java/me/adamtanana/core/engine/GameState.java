package me.adamtanana.core.engine;

public enum GameState {
    LOBBY, RUNNING, ENDING
}
