package me.adamtanana.core.engine.player;

import lombok.Getter;
import lombok.Setter;
import me.adamtanana.core.util.UtilServer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.UUID;

@Getter
public class GamePlayer {

    private UUID uuid;
    private PlayerState playerState = PlayerState.PLAYING;

    public GamePlayer(UUID uuid) {
        this.uuid = uuid;
    }

    public Player getPlayer() {
        return Bukkit.getPlayer(uuid);
    }

    public void setPlayerState(PlayerState state) {
        UtilServer.callEvent(new PlayerStateChangeEvent(Bukkit.getPlayer(uuid), playerState, state));
        playerState = state;
    }
}
