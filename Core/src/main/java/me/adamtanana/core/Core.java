package me.adamtanana.core;

import com.google.common.collect.Lists;
import lombok.Getter;
import me.adamtanana.core.command.CommandHandler;
import me.adamtanana.core.database.DatabaseEngine;
import me.adamtanana.core.event.Updater;
import me.adamtanana.core.heardbeat.Heartbeat;
import me.adamtanana.core.inventory.MenuManager;
import me.adamtanana.core.npc.NPCManager;
import me.adamtanana.core.packet.PacketModule;
import me.adamtanana.core.player.*;
import me.adamtanana.core.redis.JedisServer;
import me.adamtanana.core.redis.ServerType;
import me.adamtanana.core.redis.message.*;
import me.adamtanana.core.scoreboard.type.ScoreboardWatcher;
import me.adamtanana.core.util.*;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public abstract class Core extends JavaPlugin implements Listener {
    @Getter
    private static JedisServer jedis;
    @Getter
    private List<Module> modules = Lists.newArrayList();

    public final void onEnable() {
        new DatabaseEngine("127.0.0.1", 27017);

        jedis = new JedisServer(UtilServer.getServerName());

        JedisServer.registerListener(PlayerMessage.class, (sender, message) -> {
            Player receive = Bukkit.getPlayer(message.getPlayer());
            if (receive != null) receive.sendMessage(message.getMessage());
        });

        JedisServer.registerListener(CommandMessage.class, (sender, message) -> {
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), message.getCommand());
        });

        jedis.registerListener(PlayerOnlineMessage.class, UtilServer.getHandler());
        jedis.registerListener(GetServerName.class, (sender, msg) -> {
            System.out.println("Server name: " + msg.getName());
            jedis.setName(msg.getName());
            UtilServer.setServerName(msg.getName());
        });

        jedis.registerListener(RestartMessage.class, (sender, msg) -> UtilServer.restart());

        jedis.sendMessage(new UpdateGameData(50, false), "bungee");


        UtilWorld.init();
        UtilServer.registerListener(UtilServer.getHandler());
        CommandHandler.register(new MsgCommand());
        CommandHandler.register(new DataCommand());
        CommandHandler.register(new RestartCommand());
        UtilServer.registerListener(this);

        enableModules(new Updater(),
                new PlayerHandler(),
                new CommandHandler(),
                new MenuManager(),
                new PacketModule(),
                new Heartbeat(),
                new ScoreboardWatcher(),
                new NametagListener(),
                new NPCManager());
        enable();
        findName();

    }

    protected void enable() {
    }

    protected void disable() {
    }

    public void onDisable() {
        jedis.sendMessage(new KillServerMessage(UtilServer.getServerName()), "bungee");

        modules.forEach(Module::disable);
        disable();
    }

    public void enableModules(Module... mdls) {
        for (Module m : mdls) {
            Bukkit.getPluginManager().registerEvents(m, this);
            m.enable();
        }
        modules.addAll(Arrays.asList(mdls));
    }

    public <T extends Module> T getModule(Class<T> clazz) {
        Optional<Module> found = modules.stream().filter(c -> c.getClass().equals(clazz)).findFirst();
        if (found.isPresent()) return clazz.cast(found.get());
        return null;
    }

    public abstract ServerType getServerType();


    public final void findName() {
        new BukkitRunnable(){
            public void run () {
                if(UtilServer.getServerName().contains("-")) cancel();
                else {
                    System.out.println("Finding name!! " + UtilServer.getServerName());
                    try {
                        jedis.sendMessage(new GetServerName(getServerType(),
                                        InetAddress.getLocalHost().getHostAddress(), getServer().getPort()),
                                "bungee");
                    } catch (UnknownHostException e) {
                        e.printStackTrace();
                        Bukkit.shutdown();
                        return;
                    }
                }
            }
        }.runTaskTimer(this, 5L,  40L);
    }
}
