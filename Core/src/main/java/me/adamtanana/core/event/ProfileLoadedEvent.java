package me.adamtanana.core.event;

import lombok.AllArgsConstructor;
import lombok.Getter;
import me.adamtanana.core.player.GlobalPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.util.UUID;

@AllArgsConstructor
public class ProfileLoadedEvent extends Event {
    private static final HandlerList handlers = new HandlerList();

    @Getter
    private Player player;
    @Getter
    private GlobalPlayer gp;


    public static HandlerList getHandlerList() {
        return handlers;
    }


    public HandlerList getHandlers() {
        return handlers;
    }
}