package me.adamtanana.core.event;

import me.adamtanana.core.Module;
import me.adamtanana.core.util.UtilServer;

import java.util.Arrays;

public class Updater extends Module {

    @Override
    public void enable() {
        UtilServer.runTaskTimer(() -> {
            Arrays.asList(UpdateType.values()).stream().filter(update ->
                    update.hasElapsed()).forEach(update -> UtilServer.callEvent(new UpdateEvent(update)));
        }, 1L);
    }
}
